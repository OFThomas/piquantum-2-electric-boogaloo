/* 
 * File:   ticker.h
 * Author: jrs
 *
 * Created on 17 July 2022, 13:03
 */

#ifndef TICKER_H
#define	TICKER_H

#ifdef	__cplusplus
extern "C" {
#endif

    // System Fosc frequency
#define _XTAL_FREQ 32000000
    
    // Set the ticker period in seconds. The valid range of values is 
    // 1ms to 16ms.
#define TICK_PERIOD 5e-3 

    /**
     * @brief Setup the 1ms ticker for real-time computations
     * 
     * The ticker is used by every microcontroller process that needs to
     * perform real-time actions (e.g. displaying moving patterns on the
     * neopixel LEDs). Each process uses a common ticker, which provides
     * a function for requesting the current tick value.
     * 
     * The ticker uses timer0, which cannot be used for any other purpose.
     * 
     * Prerequisites: setup_interrupts()
     */
    void setup_tick(void);

    /**
     * @brief Increment the ticker
     * 
     * Do not use: this function is only to be called by the interrupt
     * service routine.
     */
    void tick_increment(void);

    /**
     * @brief Get the current tick value
     * 
     * The tick is incremented every 1ms. Microcontroller processes can
     * call this function to get the current tick value, and do with it
     * whatever they want (for example, wait until the tick has incremented
     * by one and then perform some action).
     * 
     * The ticker wraps back to 0 once it reaches 65535. Processes can 
     * determine how much time has elapsed by keeping a local copy of the
     * tick from the last call of this function.
     * 
     * @return The current tick value (between 0 and 65535)
     */
    unsigned tick_value(void);

    /**
     * @brief Get the true tick period
     * 
     * The period of the tick is not necessarily exactly TICK_PERIOD
     * (specified above), because of the rounding error involved in
     * setting the value of the period register. This function can be
     * used to get the actual tick period.
     * 
     * @return The true tick period 
     */
    float tick_period(void);
    
    /**
     * @brief Delay for an amount of time in seconds
     * 
     * This function returns after delaying for a certain amount of time,
     * specified as a floating point number in seconds. The time is based on
     * the ticker, and will be reasonably accurate provided that it is much
     * longer than the true tick period. 
     * 
     * @param delay value (milliseconds)
     */
    void delay(float value);

#ifdef	__cplusplus
}
#endif

#endif	/* TICKER_H */

