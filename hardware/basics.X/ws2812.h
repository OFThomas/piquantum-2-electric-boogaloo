/* 
 * File:   ws2812.h
 * Author: jrs
 *
 * Created on 14 June 2022, 16:09
 */

#ifndef WS2812_H
#define	WS2812_H

/**
 * \brief Initialise the WS2812 driver
 *
 */
void setup_ws2812(void);

/**
 * @brief Clear the sending flag for blocking write
 * 
 * Do not use: this function should only be called from the interrupt
 * service routine.
 */
void ws2812_send_complete(void);

/**
 * @brief Write a byte of data out to the WS2812 data line
 * @param data
 */
void ws2812_write(char data);

//void ws2812_write(char data);


#endif	/* WS2812_H */

