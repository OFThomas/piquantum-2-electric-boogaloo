#include <xc.h>
#include "ticker.h"

// Compute the value of the timer register required to produce
// the desired period. Assumes that the timer0 is driven by 
// Fosc/
const static unsigned tmr0 = (unsigned) (_XTAL_FREQ*TICK_PERIOD / 2048);

// Compute the true period based on the actual value written into
// TMR0
static float true_period = TICK_PERIOD;

static unsigned tick = 0;

void setup_tick(void) {

#ifdef PIC16F18346  

    // Enable the timer
    T0CON0bits.T0EN = 1;
    
    T0CON1bits.T0CS = 0x2; // Fosc/4 source

    // Set 8-bit mode
    T0CON0bits.T016BIT = 0;
    
    // Enable timer0 interrupts
    PIE0bits.TMR0IE = 1;
    
    // Set the prescaler
    T0CON1bits.T0CKPS = 0x9; // Divide by 512 (timer rate Fosc/2048)
    
    // Load the period
    TMR0H = tmr0;

#elif PIC16F18446

    TMR1CLKbits.CS = 1; // Fosc/4 source

    // Set the clock prescaler
    T1CONbits.CKPS = 0x3; // Scale to 1/8, to get Fosc/32

    // Enable read/write 16-bit mode
    T1CONbits.RD16 = 1;

    // Enable timer0 interrupts
    PIE4bits.TMR1IE = 1;

    // Enable the timer
    T1CONbits.ON = 1;

#endif
    
    // Compute the true timer period
    true_period = 2048.0 * tmr0 / _XTAL_FREQ;

}

void tick_increment(void) {
    tick++;
}

unsigned tick_value(void) {
    
    // Make a copy of the tick
    PIE0bits.TMR0IE = 0;
    unsigned tick_copy = tick;
    PIE0bits.TMR0IE = 1;
    
    return tick_copy;
}

float tick_period(void) {
    return true_period;
}

void delay(float value) {
    
    // Compute the number of ticks to wait
    unsigned diff = (unsigned)(value/true_period);
    
    // Get current tick
    unsigned start = tick_value();
    
    // You cannot write while (current < start + diff) here.
    // Exercise: why not?
    while (tick_value() - start < diff)
        ; // Wait for time to delay to elapse
}