#include <string.h>
#include "errors.h"

static char errors[ERRORS_BUFFER_SIZE];

// Total number of errors that have occurred since reset
static unsigned error_count = 0;

static struct {
    char * write;
    char * read;
} buffer_ptr = {.write = errors, .read = errors};

// Increment a pointer in the error buffer

static void ptr_inc(char ** ptr) {
    (*ptr)++;
    if ((*ptr) == errors + ERRORS_BUFFER_SIZE) {
        (*ptr) = errors;
    }
}

void setup_errors(void) {

}

static void push_char(char c) {
    errors[*buffer_ptr.write] = c;
    ptr_inc(*buffer_ptr.write);
}

static char pop_char(void) {
    char c = errors[*buffer_ptr.read];
    ptr_inc(*buffer_ptr.read);
    return c;
}

static void push_string(const char * str) {

    unsigned N = strlen(str);
    for (int n = 0; n < N; n++) {
        push_char(str[n]);
    }
    // Push a null terminator
    push_char('\0');
}

static int pop_string(char * str, unsigned N) {

    for (int n = 0; n < N; n++) {
        str[n] = pop_char();
        if (str[n] == '\0') {
            return 0;
        }
    }
    // Error, not enough space in buffer
    return -1;
}

void errors_log(const char * system, const char * msg) {

    push_string(system);
    push_string(msg);

    ++error_count;
}

int errors_get(char * system, unsigned N, char * msg, unsigned M) {

    if (buffer_ptr.read != buffer_ptr.write) {
        pop_string(system, N);
        pop_string(msg, M);
        buffer_ptr.read += strlen(system) + strlen(msg) + 2;
        return 1;
    } else {
        // No errors in buffer
        return 0;
    }
}

unsigned errors_count(void) {
    return error_count;
}