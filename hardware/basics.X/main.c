/*
 * File:   main.c
 * Author: jrs
 *
 * The purpose of this project is to get together tested code for basic
 * peripherals we need: uart, i2c, and the LED driver.
 * 
 * Created on 22 June 2022, 15:05
 */


#include <xc.h>
#include "io.h"
#include "uart.h"
#include "interrupts.h"
#include "ws2812.h"
#include "neopixel.h"
#include "ticker.h"
#include "led_test.h"
#include "terminal.h"
#include "errors.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#pragma config WDTE = OFF
#pragma config RSTOSC = HFINT32 // Use the 32MHz internal oscillator
#pragma config FEXTOSC = OFF // Otherwise RA5 won't work

#define _XTAL_FREQ 32000000

void main(void) {

    setup_interrupts();
    setup_tick();
    setup_board_io();
    setup_uart();
    setup_ws2812();
    setup_neopxl();
    setup_errors();
    setup_term();
    

    while (true) {
        term_service();
        ledtest_service(); 
    }


}
