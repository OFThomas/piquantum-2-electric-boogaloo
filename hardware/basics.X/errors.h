/* 
 * File:   errors.h
 * Author: jrs
 *
 * Created on 19 July 2022, 19:46
 */

#ifndef ERRORS_H
#define	ERRORS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define ERRORS_BUFFER_SIZE 512

    void setup_errors(void);
    
    /**
     * @brief Add an error to the buffer log
     * 
     * Use this function to push an error to the back of the error
     * circular buffer. You must ensure that the sum of the strlen of
     * the system and msg arguments is not more than ERRORS_BUFFER_SIZE-2.
     * Keep error messages as short as possible to store the maximum number
     * of errors in the buffer. Old errors will be overwritten without warning
     * if too many are added.
     * 
     * @param system The short name of the system that caused the error
     * @param msg A longer message describing the error
     */
    void errors_log(const char * system, const char * msg);

    /**
     * @brief Get an error message
     * 
     * All strings returned in the buffers are null-terminated ready
     * for printing.
     * 
     * @param system A buffer to store the system string
     * @param N The length of the system buffer
     * @param msg A buffer to store the error message string
     * @param M The length of the error message buffer
     * @return Success code. 0 means that there are no more errors in the
     * buffer. 1 means an error was returned.
     */
    int errors_get(char * system, unsigned N, char * msg, unsigned M);

    /**
     * @brief Get the number of erros that have occurred since reset
     * @return The number of errors that have occurred
     */
    unsigned errors_count(void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* ERRORS_H */

