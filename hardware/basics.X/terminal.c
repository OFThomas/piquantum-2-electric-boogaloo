#include <string.h>
#include <stdio.h>
#include <xc.h>

#include "uart_linebuffer.h"
#include "ticker.h"
#include "led_test.h"
#include "terminal.h"
#include "errors.h"

void setup_term(void) {

#ifdef INTERACTIVE_MODE
    puts("\n\n\n---------------------------------------------");
    puts("Welcome to the PiQuantum 2: Electric Boogaloo");
    puts("PIC UART interface");
    puts("---------------------------------------------\n");

    puts("Commands can be typed and edited with backspace,");
    puts("but you cannot edit using arrow keys. Lines are at");
    puts("most 128 characters long and there is no bounds checking");
    puts("currently, so do not type a line longer than that.\n");

    puts("Type 'help' to see the list of available commands.\n");
#endif
}

const static char * version = "0.1_under_development";
static char error_system[ERROR_SYSTEM_LEN];
static char error_msg[ERROR_MSG_LEN];

static enum {
    PUT_PROMPT,
    IDLE,
    PROCESS,
} state = PUT_PROMPT;

static void process_command(char * cmd) {

    const char * tok = " ";

    char * ptr = strtok(cmd, tok);

    // Check for no command
    if (ptr == NULL) {
        // Do nothing and return
        return;
    } else if (strcmp(ptr, "help") == 0) {
        puts("Available commands:");
        puts("help - print this summary");
        puts("version - print the embedded software version");
        puts("error [count|print] - get error count and print next message");
        puts("reset - perform a software reset of this device");
        puts("ledtest [off|1|2] [float]- perform a preset LED test");
    } else if (strcmp(ptr, "version") == 0) {
        printf("%s\n", version);
    } else if (strcmp(ptr, "reset") == 0) {
        puts("Performing a software reset in 1s.");
        delay(1);
        RESET();
    } else if (strcmp(ptr, "error") == 0) {

        // Check which LED test is requested
        ptr = strtok(NULL, tok);
        if (ptr == NULL) {
            puts("error: requires argument. Type 'help' for synopsis");
        } else if (strcmp(ptr, "count") == 0) {
            printf("Error count: %u\n", errors_count());
        } else if (strcmp(ptr, "print") == 0) {
            int status = errors_get(error_system, ERROR_SYSTEM_LEN,
                    error_msg, ERROR_MSG_LEN);
            if (status == 1) {
                printf("Error from %s: %s\n", error_system, error_msg);
            } else if (status == 0) {
                puts("No errors found in the error buffer");
            }
        }
        
    } else if (strcmp(ptr, "ledtest") == 0) {

        // Check which LED test is requested
        ptr = strtok(NULL, tok);
        if (ptr == NULL) {
            puts("ledtest: requires arguments. Type 'help' for synopsis");
        } else if (strcmp(ptr, "1") == 0) {
            ptr = strtok(NULL, tok);
            float period = atof(ptr);
            ledtest_enable(ALL_CYCLE);
            ledtest_set_period(period);
            puts("Starting LED test ALL_CYCLE");
        } else if (strcmp(ptr, "2") == 0) {
            ptr = strtok(NULL, tok);
            float period = atof(ptr);
            ledtest_enable(RUNNING_LIGHT);
            ledtest_set_period(period);
            puts("Starting LED test RUNNING_LIGHT");
        } else if (strcmp(ptr, "off") == 0) {
            ledtest_enable(OFF);
            puts("Stopping LED test");
        } else {
            printf("ledtest: %s: argument not recognised\n", ptr);
        }

    } else {
        printf("%s: command not found\n", ptr);
    }

}

void term_service(void) {

    // Buffer for commands
    static char cmd[128];

    switch (state) {
        case PUT_PROMPT:
            // Write the command prompt and leave
#ifdef INTERACTIVE_MODE
            printf("$ ");
#endif
            state = IDLE;
            break;
        case IDLE:
            // Check for a line in the line buffer
            if (uart_line_received() == true) {
                gets(cmd);
                state = PROCESS;
            }
            break;
        case PROCESS:
            // Process the command
            process_command(cmd);
            state = PUT_PROMPT;
            break;
    }
}
