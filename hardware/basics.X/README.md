# Basic peripheral libraries

This project (basics.X) contains library development for the peripherals we might need.

## UART

The intention is to communicate between a raspberry pi and the pic16f using the uart link.

### Raspberry pi 3b+ UART loopback test

To get the serial interface set up on the raspberry pi 3b+, follow these steps:
* Install Raspbian
* Run ```sudo raspi-config```
* Navigate to ```Interface Options``` and then ```Serial Port```
* Disable login shell over serial port (to free up the serial port for other uses), and enable the serial port hardware.
* Reboot the raspberry pi
* Check that ```/dev/serial0``` exists.

Next, check that the serial port works using minicom. If minicom is not installed, run ```sudo apt install minicom```.

Then connect pins 8 and 10 of the GPIO together. Pin 8 (GPIO 14) is TX, and pin 10 (GPIO 15) is RX. Data from minicom will be sent out from TX and received on RX.

Run:

```bash
minicom -D /dev/serial0
```

Typed characters should be echoed back onto the screen. If the wire between TX and RX is disconnected, characters will not be echoed back. If all this works, then the UART interface is working.

Unfortunately the mini-UART (/dev/ttyS0) on the raspberry pi 3b+ is not reliable, so it is necessary to use the full UART interface normally attached to the bluetooth module. To do this, disable bluetooth by adding

```
dtoverlay=disable-bt
```

to the file ```/boot/config.txt``` (see ```/boot/overlays/README``` for more information). After restarting the raspberry pi, check that ```/dev/serial0``` points to ```/dev/ttyAMA0```, which is the full UART device. Continue to use ```/dev/serial0``` in applications.

### To test EUSART between Pi and Pic 

Use one of these two commands to open a UART console

```bash
minicom -D /dev/serial0 -b 9600
cu -l /dev/serial0 -s 115200
```

Both these terminals send carriage return when the enter key is pressed, which the PIC translates to CRNL (see termios manual, ICRNL setting for more information).

### UART API

#### Targets for the API

This is the overall specification for what the API should provide.

* Only the raspberry pi can initiate communication. Information from the pic -> raspberry pi should be requested (like a VISA serial device).
* The interface should be low speed/not have complicated/real-time timing requirements
* The raspberry pi communicates with one logical device, rather than each board individually. 
* Allow the raspberry pi to request the number of boards, the physical layout of the boards, and maybe the locations of the LEDs.
* Set all LEDs RGB values individually (statically, immediately)
* The microcontroller should have certain static presets (for LED patterns)
  * All the same colour
  * All off
  * Horizontal/vertical lines
  * Circles/ellipses
  * Squares/rectangles
  * Shapes of optical elements
  * Arbitrary polygons
* Overlaid on the static pattern should be dynamic LED patterns, controlled by the microcontroller
  * First type: take a "pulse" shape (say a Gaussian intensity profile) and propogate it along a predefined path, at a particular fixed rate (or traversing the whole path in a certain defined time).
  * Second type: propogate same as before, but allow the intensity profile and shape of the pulse to change.

* Static patterns should be specified first, and will be executed immediately (because this will probably not look like a visible time delay).
* Dynamic patterns should be sent to the MCU one at a time.
* Run command: then the "script" (queue, FIFO command buffer) can be executed using a run command, which starts the dynamic pattern.
* Pause command: stop the buffer executing
* Repeat command: just keep repeating the script
* Commands could be pushed back while the script is running
* Query command to check whether the dynamic pattern has finished, or where it is up to
* Cancel command to stop the dynamic pattern from executing. 
* Clear command to clear the script
* Reset command to reset the state of the MCU
