#include "uart_linebuffer.h"
#include "uart.h"
#include <stdbool.h>

char line_buffer[UART_LINE_BUFFER_SIZE];

static struct {
    
    // The location of the next character to be read and returned by 
    // uart_getche(). This cannot ever advance further than the 
    // current_line pointer below.
    char * char_read;
    
    // A pointer to the location where the next character
    // should be written in the current line. 
    char * char_write;
    
    // A pointer to the line which is currently being
    // edited, and which has not yet been submitted.
    char * current_line;
    
} line_buffer_ptr = { 
    .char_read = line_buffer,
    .char_write = line_buffer,
    .current_line = line_buffer
}; 

// 
bool uart_line_received(void) {
    return (line_buffer_ptr.char_read < line_buffer_ptr.current_line);
}

// Increment a pointer in the line buffer (ring buffer)
static void ptr_inc(char ** ptr) {
    (*ptr)++;
    if ((*ptr) == line_buffer + UART_LINE_BUFFER_SIZE) {
        (*ptr) = line_buffer;
    }
}

// Decrement a pointer in the line buffer (ring buffer)
static void ptr_dec(char ** ptr) {

    if ((*ptr) == line_buffer) {
        (*ptr) = line_buffer + UART_LINE_BUFFER_SIZE;
    }
    (*ptr)--;
}

/// Add a character to the line buffer
static void push_char(char c) {
    *line_buffer_ptr.char_write = c;
    ptr_inc(&line_buffer_ptr.char_write);
    // Echo the character
    uart_write(c);
}

/// Add a character to the line buffer
static char pop_char() {
    char c = *line_buffer_ptr.char_read;
    ptr_inc(&line_buffer_ptr.char_read);
    return c;
}

// Use this subroutine to submit the current line
static void submit_line() {
    // Add the terminating linefeed
    push_char('\n');
    // Store the start of the (now current) line
    line_buffer_ptr.current_line = line_buffer_ptr.char_write;
    // Echo the newline (only need to send carriage return,
    // push_char echoes the linefeed).
    uart_write('\r');
}

// When backspace is received, edit the line buffer accordingly
static void backspace() {
    // If already the start of the current line, do nothing.
    // Otherwise, move the write pointer back one space
    if (line_buffer_ptr.char_write != line_buffer_ptr.current_line) {
        ptr_dec(&line_buffer_ptr.char_write);
        // Echo the backspace 
        uart_write('\b');
        uart_write(' ');
        uart_write('\b');
    }
}

void uart_process_char(char c) {
    
    switch (c) {
        // For either a newline or carriage return, store a newline
        // and submit the line
        case '\n':
        case '\r':
            submit_line();
            break;
        // For backspace, if not at the start of the line, move the
        // character pointer one space backwards.
        case '\b':
            backspace();
            break;
        // For all other characters, store the character in the
        // line buffer.
        default:
            push_char(c);
            break;
    }
    
}

char uart_getche(void) {
    while (!uart_line_received())
        ; // Wait for a line to be submitted
    
    // Return the next character from the line buffer
    return pop_char();
}
