/* 
 * File:   uart_linebuffer.h
 * Author: jrs
 *
 * Characters that are received via UART are added to a line buffer, that can
 * be edited, before they are forwarded to the likes of getchar, etc. when the
 * user presses the enter key.
 * 
 * The line buffer implementation here uses a ring buffer of a fixed size, and
 * lines are placed in this buffer, separated by linefeed (newline, '\n')
 * characters. This way, the buffer can support many short lines, or fewer long
 * lines, or a mixture of the two. This buffer may be thought of as the input
 * stream buffer; user software has no conception of lines, only characters
 * in this buffer. The main effect of the line buffer is that characters are
 * made available one line at a time, not one character at a time. The buffer 
 * is still accessed character by character, using uart_getche().
 * 
 * Software must read from the line buffer before two many lines are added,
 * otherwise data will be overwritten when new lines are added.
 * 
 * Software that eventually calls getche() will only return when a line has
 * been "submitted", which means that the user has pressed the enter key. At
 * that point, a call to uart_getche() will return a pointer to the next
 * line to read (a linefeed-terminated string). Before the user presses enter,
 * the user may edit the line by pressing backspace, which will modify the
 * previously written contents of the current line.
 * 
 * Created on 28 June 2022, 20:14
 */

#ifndef UART_LINEBUFFER_H
#define	UART_LINEBUFFER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdbool.h>
    
#define UART_LINE_BUFFER_SIZE 128
    
    /**
     * @brief Get the next character in the line buffer
     * 
     * From the point of view of user software, there are no lines; only
     * characters. A call to this function is basically the same as a call
     * to getchar, but the character will only be returned if the line that
     * it is part of has been submitted. When it has, the character is 
     * returned. Repeated calls will return each character of the line
     * one by one, including the final terminating linefeed.
     * 
     * This function blocks if no line has been submitted (the end of the
     * input stream has been reached).
     * 
     * @return The next character in the input stream
     */
    char uart_getche(void);
    
    /**
     * @brief Check whether a line has been received 
     * 
     * This function makes a non-blocking check for whether a line has been
     * received into the line buffer. If there is a line, the function returns
     * true; else it returns false;      
     */
    bool uart_line_received(void);

    /**
     * @brief Process a received character and store in the line buffer
     * 
     * Do not use: this function is designed to be called from the
     * interrupt service routine when a character is received over UART.
     * 
     * This function takes a character received via UART and uses it to
     * modify the line buffer. Different things happen depending on what
     * character is passed.
     * 
     * If a regular (printable) character is passed, then it is stored at
     * the end of the line buffer.
     * 
     * If a newline or carriage return is received, then a newline (only)
     * is pushed to the back of the line buffer, and that line is marked
     * as complete (a call to uart_getche() will return characters from the
     * line).
     * 
     * If a backspace ('\b') is received, then the character pointer is 
     * moved one place backwards in the line buffer, so that the next character
     * which is received overwrites the previous character received. If the
     * character pointer is already at the start of the current line, then
     * no action is taken.
     * 
     * @param c The character received over UART
     */
    void uart_process_char(char c);
    

#ifdef	__cplusplus
}
#endif

#endif	/* UART_LINEBUFFER_H */

