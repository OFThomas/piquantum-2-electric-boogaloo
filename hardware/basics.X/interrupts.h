/* 
 * File:   interrupts.h
 * Author: jrs
 *
 * Created on 23 June 2022, 15:23
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C" {
#endif

    /**
     * @brief Setup and enable interrupts globally
     * 
     * Prerequisites: none
     * 
     * This function enables interrupts globally, and enables
     * peripheral interrupts. To enable a peripheral interrupt, 
     * make sure this function has been called, and then set 
     * the interrupt enable bit in the correct PIEx register 
     * for the peripheral.
     * 
     * 
     */
    void setup_interrupts(void)
    {
        // Enable peripheral interrupts
        INTCONbits.PEIE = 1;
        
        // Globally enable interrupts
        INTCONbits.GIE = 1;
    }


#ifdef	__cplusplus
}
#endif

#endif	/* INTERRUPTS_H */

