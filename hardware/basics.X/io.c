#include <xc.h>

void setup_board_io(void) {
    // Clear the latch data in preparation
    LATAbits.LATA5 = 0; // D4 = RA5
    LATAbits.LATA0 = 0; // D5 = RA0 (PGEC)
    LATAbits.LATA2 = 0; // D6 = RA2
    LATCbits.LATC5 = 0; // D7 = RC5

    // Set all the pins as digital
    ANSELAbits.ANSA5 = 0;
    ANSELAbits.ANSA0 = 0;
    ANSELAbits.ANSA2 = 0;
    ANSELCbits.ANSC5 = 0;

    // Set the digital direction as output
    TRISAbits.TRISA5 = 0;
    TRISAbits.TRISA0 = 0;
    TRISAbits.TRISA2 = 0;
    TRISCbits.TRISC5 = 0;
}

/**
 * @brief Turn all the LEDs on or off
 * @param state 1 for on, 0 for off. Other values undefined
 */
void set_all_leds(unsigned char state) {
    LATAbits.LATA5 = state;
    LATAbits.LATA0 = state;
    LATAbits.LATA2 = state;
    LATCbits.LATC5 = state;
}

void toggle_d4(void) {
    LATAbits.LATA5 = ~LATAbits.LATA5;
}

void toggle_d6(void) {
    LATAbits.LATA2 = ~LATAbits.LATA2;
}