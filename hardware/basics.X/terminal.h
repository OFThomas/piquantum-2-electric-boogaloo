/* 
 * File:   terminal.h
 * Author: jrs
 *
 * Created on 19 July 2022, 13:19
 */

#ifndef TERMINAL_H
#define	TERMINAL_H

#ifdef	__cplusplus
extern "C" {
#endif

    // Comment this line to use automatic mode. In automatic mode, no
    // header text is printed when the microcontroller is powered on or
    // reset, and no prompt is printed. Automatic mode is intended for use
    // with a programmed interface (it should be easier to parse responses).
#define INTERACTIVE_MODE
    
#define ERROR_SYSTEM_LEN 15
#define ERROR_MSG_LEN    128
    
    /**
     * @brief Setup the PIC UART terminal interface
     * 
     * Prerequisites: setup_uart(), setup_tick();
     */
    void setup_term(void);

    /**
     * @brief Receive and respond to commands  
     * 
     * Do not use: this function should only be called inside the service
     * loop, not from any other application functions.
     * 
     * This function should be called in the service loop as regularly as 
     * possible.
     */
    void term_service(void);

#ifdef	__cplusplus
}
#endif

#endif	/* TERMINAL_H */

