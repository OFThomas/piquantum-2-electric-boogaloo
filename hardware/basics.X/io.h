/* 
 * File:   io.h
 * Author: jrs
 *
 * Created on 22 June 2022, 15:24
 */

#ifndef IO_H
#define	IO_H


#ifdef	__cplusplus
extern "C" {
#endif

    /**
     * @brief Run before using the development board IO functions
     * 
     * Prerequisites: none
     */
    void setup_board_io(void);
    

    /**
     * @brief Turn all the LEDs on or off
     * @param state 1 for on, 0 for off. Other values undefined
     */
    void set_all_leds(unsigned char state);

    /**
     * @brief Toggle the state of D4
     * 
     * If D4 is off, turn it on. If it is on, turn it off.
     */
    void toggle_d4(void);
    
    void toggle_d6(void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* IO_H */

