#include <xc.h>
#include <stdbool.h>

#define WS2812_BUFFER_SIZE 16

void setup_ws2812(void) {

#ifdef PIC16F18346
    // Initialise PWM5
    T2CON = 0x04;
    PR2 = 5;

    // Set the PWM to active low
    PWM5CON = 0x80;

    // Set the duty cycle (10-bit)
    PWM5DCH = 0x3;
    PWM5DCL = 0x0;

    // Set up serial peripheral
    SSP1CON1 = 0x23;

    // Set up the CLC to perform the following operation: 
    // OUT = SCL x (SDA + PWM)

    // Set up the (three) inputs to the CLC
    CLC1SEL0 = 18; // D1 = SCL1 (the clock line) 
    CLC1SEL1 = 19; // D2 = SDA1 (the data line) 
    CLC1SEL2 = 16; // D3 = PWM5 output
    CLC1SEL3 = 0;

    // Set up the data gating stage. Note that there is a typo in the
    // datasheet -- the gate stage implements an OR gate when all inputs
    // and outputs are non-inverted, not an AND gate. 
    CLC1GLS0 = 0x02; // lc1g1 = D1
    CLC1GLS1 = 0x28; // lc1g2 = D2 + D3
    CLC1GLS2 = 0; // lc1g3 = 1 (so that AND will work)
    CLC1GLS3 = 0; // lc1g4 = 1
    CLC1POL = 0xc; // This sets the output negation used in lc1g2,3,4

    // Set up the peripheral pin mapping
    RC7PPS = 0x4;
    TRISCbits.TRISC7 = 0;

    // Set up the logic function: perform logical 
    // AND between the gate outputs lc1gN
    CLC1CON = 0x82;

    // Enable transmit interrupts here
    PIE1bits.SSP1IE = 1;
    
#elif PIC16F18446
  
    // Initialise PWM5
    T2CON = 0x04;
    PR2 = 5;

    // Set the PWM to active low and enable
    PWM6CON = 0x80;

    // Set the duty cycle
    PWM6DCH = 0x3;
    PWM6DCL = 0x0;

    // Set up serial peripheral
    SSP1CON1 = 0x23;

    // Set up the CLC to perform the following operation: 
    // OUT = SCL x (SDA + PWM)

    // Set up the (three) inputs to the CLC
    CLC1SEL0 = 39; // D1 = SCL1 (MSSP1_clk_out, the clock line) 
    CLC1SEL1 = 38; // D2 = SDA1 (MSSP1_data_out, the data line) 
    CLC1SEL2 = 24; // D3 = PWM6 output (PWM6_out)
    CLC1SEL3 = 0;

    // Set up the data gating stage. Note that there is a typo in the
    // datasheet -- the gate stage implements an OR gate when all inputs
    // and outputs are non-inverted, not an AND gate. 
    CLC1GLS0 = 0x02; // lc1g1 = D1
    CLC1GLS1 = 0x28; // lc1g2 = D2 + D3
    CLC1GLS2 = 0; // lc1g3 = 1 (so that AND will work)
    CLC1GLS3 = 0; // lc1g4 = 1
    CLC1POL = 0xc; // This sets the output negation used in lc1g2,3,4

    // Set up the peripheral pin mapping
    RC7PPS = 0x1;
    TRISCbits.TRISC7 = 0;

    // Set up the logic function: perform logical 
    // AND between the gate outputs lc1gN
    CLC1CON = 0x82;

    // Enable transmit interrupts here
    PIE3bits.SSP1IE = 1;
    
#endif
}

/// True when a byte is currently being sent. False otherwise.
static bool sending = false;

/**
 * @brief A transmit buffer for ws2812 bytes characters
 */
static char tx_buffer[WS2812_BUFFER_SIZE];

/**
 * @brief A structure for holding the next available TX buffer entry
 */
static struct {
    char * read;
    char * write;
} tx_ptr = {.read = tx_buffer, .write = tx_buffer };

void ws2812_write(char data) {
    
    // Store the data
    *tx_ptr.write = data;
    
    // Update the write pointer
    tx_ptr.write++;
    if (tx_ptr.write == tx_buffer + WS2812_BUFFER_SIZE) {
        tx_ptr.write = tx_buffer;
    }
    
    // If no byte is currently being sent, then send this byte (which
    // is the first). This logic hinges on the sending flag only being
    // false when all the data in the buffer has finished being sent.
    if (sending == false) {
        sending = true;
        SSP1BUF = data;
    }
}

void ws2812_send_complete(void) {
    
    // Update the read pointer
    tx_ptr.read++;
    if (tx_ptr.read == tx_buffer + WS2812_BUFFER_SIZE) {
        tx_ptr.read = tx_buffer;
    }
    
    // Check if there is still data to send (note there is not buffer
    // overflow check here)
    if (tx_ptr.read != tx_ptr.write) {      
        // Send the next byte of data
        SSP1BUF = *tx_ptr.write;
    } else {
        // There is no more data to send
        sending = false;
    }
}