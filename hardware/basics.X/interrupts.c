#include <xc.h>
#include "io.h"
#include "uart.h"
#include "uart_linebuffer.h"
#include "ws2812.h"
#include "neopixel.h"
#include "ticker.h"

/**
 * @brief Global interrupt service routine.
 * 
 * This function is called automatically when any interrupt
 * is raied in the PIC16f, provided that setup_interrupts() has
 * been run.
 * 
 * 
 */
void __interrupt() isr(void) {

    // Check for UART RX interrupt
#ifdef PIC16F18346
    if (PIR1bits.RCIF == 1) {
#elif PIC16F18446
    if (PIR3bits.RC1IF == 1) {       
#endif
        char c = RC1REG; // Received char
        uart_process_char(c); // Store to the line buffer
    }

    // Check for UART TX interrupt
#ifdef PIC16F18346
    if (PIR1bits.TXIF == 1) {
#elif PIC16F18446
    if (PIR3bits.TX1IF == 1) {
#endif
        // Attempt to send out data from the transmit buffer
        // over UART
        uart_pop_tx();
    }

    // Check if the SPI transmit buffer is free
#ifdef PIC16F18346
    if (PIR1bits.SSP1IF == 1) {     
        ws2812_send_complete();
        PIR1bits.SSP1IF = 0; // Clear the flag
    }
#elif PIC16F18446
    if (PIR3bits.SSP1IF == 1) {
        ws2812_send_complete();
        PIR3bits.SSP1IF = 0; // Clear the flag
    }
#endif

    // Check for the timer 1 interrupt
#ifdef PIC16F18346
    if (PIR1bits.TMR1IF == 1) {
        PIR1bits.TMR1IF = 0; // Clear the interrupt
        neopxl_isr();
    }
#elif PIC16F18446
    if (PIR4bits.TMR1IF == 1) {
        PIR4bits.TMR1IF = 0; // Clear the interrupt
        neopxl_isr();
    }     
#endif

    // Check for the timer 0 interrupt
#ifdef PIC16F18346
    if (PIR0bits.TMR0IF == 1) {
        tick_increment();
        toggle_d4();
        PIR0bits.TMR0IF = 0; // Clear the interrupt
    }
#elif PIC16F18446
    // TODO
#endif  

    return;
}
