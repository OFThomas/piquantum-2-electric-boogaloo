/* 
 * File:   uart.h
 * Author: jrs
 *
 * The functions here provide a UART interface on pins RB5 and RB6 of the
 * microcontroller, for baud rate 9600:
 * 
 * PIC   RB5 (Tx) ----- (Rx) GPIO15 (10)   Raspberry PI 
 *       RB6 (Rx) ----- (Tx) GPIO14 (8)
 * 
 * You must also remember to connect the grounds of the PIC and Raspberry Pi
 * together.
 * 
 * Created on 22 June 2022, 17:05
 */

#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    /**
     * @brief Setup UART serial connection
     * 
     * Prerequisites: setup_interrupts()
     */
    void setup_uart(void);
    

    /**
     * @brief Send data via uart (blocking)
     * @param data The character to send via uart
     * 
     * Prerequisites: setup_uart()
     * 
     * The function will send data as soon as the transmit
     * buffer is available, and then return immediately.
     */
    void uart_blocking_write(char data);

    /**
     * @brief Send a byte using UART TX
     * 
     * This is a non-blocking call. The data will be pushed to a 
     * transmit buffer, and transmitted when the UART peripheral 
     * has finished transmitting all previous bytes. It is important
     * to ensure that the buffer is not overfilled, otherwise data
     * will be overwritten. Therefore, the function must not be called
     * at a continuous rate greater than that supported by the UART
     * output baud rate.
     * 
     * The function will return immediately, and data will be sent
     * soon after.
     * 
     * @param c The byte to send 
     */
    void uart_write(char c);
    
    /**
     * @brief Push a character to the back of the receive buffer
     * 
     * Do not call: This function is intended for use inside the 
     * interrupt service routine.
     * 
     * This function does not perform error checking, and will overwrite
     * the buffer without warning if characters have not been read
     * quickly enough.
     * 
     * @param c The character just received
     */
    void uart_push_rx(char c);
    
    /**
     * @brief Pop a character from the front of the TX buffer
     * 
     * Do not call: This function is intended for use inside the 
     * interrupt service routine.
     * 
     * When this function is called, a byte from the transmit queue
     * is sent out on the UART TX and the byte is popped from the
     * transmit queue.
     * 
     * This function does not perform error checking, and will overwrite
     * the buffer without warning if characters have not been read
     * quickly enough.
     * 
     * @return The character obtained from the buffer
     */
    void uart_pop_tx(void);

    /**
     * @brief Get the next character from the RX buffer
     * 
     * Prerequisites: setup_uart()
     * 
     * This function may be used by the application. If data has
     * been received by the UART peripheral, then it can be retrieved
     * using this function. The function will return 1 if the data has
     * been stored in the argument pointer; else zero is returned for
     * no data.
     * 
     * @param p A pointer to the returned data
     * @return 1 if data is valid, 0 if no data in buffer
     */
    int uart_read(char * p);
    
    /**
     * @brief UART echo service
     * 
     * Prerequisites: setup_uart()
     * 
     * This function will realise an echo function on the
     * UART peripheral: any data received on RX will be sent
     * immediately to TX.
     * 
     * Call this function repeatedly as part of a main loop
     * servicing all microcontroller functions. The function 
     * will not block.
     * 
     */
    
#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

