/* 
 * File:   led_test.h
 * Author: jrs
 *
 * Created on 17 July 2022, 16:01
 */

#ifndef LED_TEST_H
#define	LED_TEST_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdbool.h>

    // The default period for the LED tests
#define LEDTEST_PERIOD 1
    
    // Define the acceptable delay before the led test service starts
    // running after the period has expired (seconds)
#define LEDTEST_TOL 5e-3

    typedef enum {
        /**
         * Set all the LEDs successively to R->G->B and repeat.
         * The period is the time between colour changes.
         */
        ALL_CYCLE,

        /**
         * Perform a running light effect across the LED strip
         * alternating between the colours R->G->B. The period is
         * the time for one running light effect (of one colour)
         * to cross the display.
         */
        RUNNING_LIGHT,
        OFF
    } ledtest_t;

    /**
     * @brief Perform a basic flashing LED test on the whole neopixel strip
     * 
     * Do not use: this function should only be called inside the service
     * loop, not from any other application functions.
     * 
     * This function should be called in the service loop as regularly as 
     * possible.
     */
    void ledtest_service(void);

    /**
     * @brief Enable or disable an LED
     * @param state True to enable the service, false otherwise
     */
    void ledtest_enable(ledtest_t test);

    /**
     * @brief Set a new period for the current test
     * 
     * What the period means depends on the test. See the ledtest_t
     * enum for descriptions.
     * 
     * @param period The new period for the current test
     */
    void ledtest_set_period(float period);




#ifdef	__cplusplus
}
#endif

#endif	/* LED_TEST_H */

