#include <xc.h>

#include "neopixel.h"
#include "ws2812.h"

static uint8_t framebuffer[3 * NUM_PIXELS];

#define GREEN 0
#define RED 1
#define BLUE 2

// This is the number of Fosc/32 ticks in one frame time, which is the
// time between the start of one frame and the start of the next.
// At 32MHz Fosc, Fosc/32 is a 1us clock. The frame_period must not
// exceed 65536 us so as to fit in a 16-bit register. 
const static unsigned frame_period = _XTAL_FREQ / (32 * FRAME_RATE);

// This is the number of Fosc/128 ticks between successive bytes of
// the framebuffer.
const static unsigned byte_gap = (unsigned) (BYTE_DELAY*_XTAL_FREQ / 32);

// This is the delay between the end of one framebuffer and the start
// of the next. It is up to you to ensure that this is not negative
// (at the moment). For this, check that the framerate, number of pixels,
// and byte delay are simultaneously too large. Units Fosc/128 ticks.
const static unsigned frame_gap = frame_period - (3 * NUM_PIXELS - 1) * byte_gap;

// This index is used to keep track of which byte of the framebuffer should 
// be transmitted next.
static unsigned framebuffer_offset = 0;

void setup_neopxl(void) {

    // The timer must be set to a period which generates a refresh 
    // rate of approximately 60Hz across the whole display. Data must be
    // sent to the neopixels one framebuffer at a time, with at least a
    // 50us delay between framebuffers. The bytes of the framebuffer may
    // be delayed by up to 40us (to leave tolerance before the main latching
    // 50us). Each byte is sent in a separate interrupt event, triggered by
    // timer0. The purpose of using separate interrupts for each byte,
    // separated by a large gap, is to allow the microcontroller time to 
    // perform other tasks even while the display is continuously updated.
    //
    // For a small number of neopixels, padding may need to be added
    // between framebuffers, increasing the minimum 50us, in order to 
    // realise the target 60Hz refresh rate. For a large number of neopixels,
    // it may be necessary to reduce the 40us gap between bytes. This gap
    // cannot be reduced arbitrarily though, in order to ensure the 
    // microcontroller has time to perform other tasks.

#ifdef PIC16F18346  
    T1CONbits.TMR1CS = 0; // Fosc/4 source

    // Set the clock prescaler
    T1CONbits.T1CKPS = 0x3; // Scale to 1/8, to get Fosc/32

    // Enable timer1 interrupts
    PIE1bits.TMR1IE = 1;

    // Enable the timer
    T1CONbits.TMR1ON = 1;

#elif PIC16F18446

    TMR1CLKbits.CS = 1; // Fosc/4 source

    // Set the clock prescaler
    T1CONbits.CKPS = 0x3; // Scale to 1/8, to get Fosc/32

    // Enable read/write 16-bit mode
    T1CONbits.RD16 = 1;

    // Enable timer1 interrupts
    PIE4bits.TMR1IE = 1;

    // Enable the timer
    T1CONbits.ON = 1;

#endif

}

// Write a 16-bit period value to the timer

static void write_period(unsigned value) {

    // The timer increments the TMR0H/L pair from its written
    // value up to 0xffff, so the period of the timer is 
    // given by this difference.
    TMR1 = 0xffff - value;
}

/**
 * @brief Send the next byte of the framebuffer
 * 
 * 
 */
void neopxl_isr(void) {

    // Send the current byte of the framebuffer
    ws2812_write(framebuffer[framebuffer_offset]);

    // Prepare the next delay
    if (framebuffer_offset == 3 * NUM_PIXELS) {
        // Reached the end of the framebuffer. Prepare the latching delay.
        write_period(frame_gap);
        framebuffer_offset = 0;
    } else {
        // More bytes in the framebuffer
        write_period(byte_gap);
        ++framebuffer_offset;
    }
}

void neopxl_write(unsigned addr, colour_t colour) {

    // If address out of range, do nothing
    if (addr > 3 * NUM_PIXELS) {
        return;
    }

    // Write the LED colour to the framebuffer
    framebuffer[3 * addr + RED] = colour.red;
    framebuffer[3 * addr + GREEN] = colour.green;
    framebuffer[3 * addr + BLUE] = colour.blue;
}

const colour_t COL_RED = {.red = BRIGHT, .green = 0, .blue = 0};
const colour_t COL_GREEN = {.red = 0, .green = BRIGHT, .blue = 0};
const colour_t COL_BLUE = {.red = 0, .green = 0, .blue = BRIGHT};

const colour_t COL_WHITE = {.red = BRIGHT, .green = BRIGHT, .blue = BRIGHT};
const colour_t COL_OFF = {.red = 0, .green = 0, .blue = 0};