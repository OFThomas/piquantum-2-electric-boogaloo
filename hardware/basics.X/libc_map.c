/**
 * @file libc_map.c
 * @brief File of interfaces for use by the C standard library
 * 
 */

#include "uart.h"
#include "uart_linebuffer.h"

/**
 * @brief Define putch as uart_write for the purpose of printf in stdio.h
 * 
 * Prerequisites: To use printf from stdio.h, you must called setup_uart()
 * first, so that uart_write works.
 * 
 * Performs NL -> CR+NL translation.
 * 
 * @param c The character to print to stdout (over uart)
 */
void putch(char c) {
    if (c == '\n') {
        uart_write('\r'); // Prepend a carriage return
    }
    uart_write(c);
}

/**
 * @brief Wait for a character from stdin (uart), echo it, and return it
 * 
 * The echo happens automatically as part of the uart reception, so there
 * is no explicit echo here.
 * 
 * @return The next character from the buffer
 */
char getche(void) {
    return uart_getche();
}

int getch(void) {
    return getche();
}