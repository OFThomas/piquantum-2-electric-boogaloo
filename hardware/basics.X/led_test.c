#include "ticker.h"
#include "led_test.h"
#include "neopixel.h"
#include "io.h"
#include "errors.h"

static enum {
    RED,
    GREEN,
    BLUE,
} state = RED;

static ledtest_t ledtest_test = OFF;

static float ledtest_period = LEDTEST_PERIOD;

// Enabling a test will write the period
static unsigned diff = 0;

// The tolerance for the main loop
const static unsigned tol = (unsigned) (LEDTEST_TOL / TICK_PERIOD);

/// The last tick value seen by the service
static unsigned tick_last = 0;

static void ledtest_set_all_leds(colour_t colour) {
    for (unsigned n = 0; n < NUM_PIXELS; n++) {
        neopxl_write(n, colour);
    }
}

static bool ledtest_on = false;

// Expects to be called once each ledtest_period

static void ledtest_all_cycle(void) {

    // Then toggle the LED state
    switch (state) {
        case RED:
            state = GREEN;
            ledtest_set_all_leds(COL_RED);
            break;
        case GREEN:
            state = BLUE;
            ledtest_set_all_leds(COL_GREEN);
            break;
        case BLUE:
            state = RED;
            ledtest_set_all_leds(COL_BLUE);
            break;
    }
}

static void error_tolerance_exceeded(void) {
    set_all_leds(1);
    errors_log("ledtest", "service latency exceeded.");
    ledtest_enable(OFF);
}

static void ledtest_running_light(void) {

    static unsigned index = 0;
    static unsigned on = 1; // Flag for on or off
    static colour_t col;

    // Set the colour at the start of a cycle
    if (index == 0 && on == 1) {
        switch (state) {
            case RED:
                state = GREEN;
                col = COL_RED;
                break;
            case GREEN:
                state = BLUE;
                col = COL_GREEN;
                break;
            case BLUE:
                state = RED;
                col = COL_BLUE;
                break;
        }
    }

    // Set the state of the light
    if (on == 1) {
        neopxl_write(index++, col);
    } else {
        neopxl_write(index++, COL_OFF);
    }

    // Check whether the end of the strip is reached
    if (index == NUM_PIXELS) {
        index = 0;
        on ^= 1;
    }

}

void ledtest_service(void) {

    if (ledtest_test == OFF) {
        return;
    }

    unsigned tick_current = tick_value();

    if (tick_current - tick_last > diff) {

        // Check whether the tolerance is exceeded
        if (tick_current - tick_last > diff + tol) {
            // Error -- tolerance exceeded
            error_tolerance_exceeded();
        }

        // Store the new tick value. 
        tick_last += diff;

        // Run one of the tests
        switch (ledtest_test) {
            case ALL_CYCLE:
                ledtest_all_cycle();
                break;
            case RUNNING_LIGHT:
                ledtest_running_light();
                break;
            case OFF:
                break;
        }

    }
}

// Update the diff value based on the period and test.
// Call this function after updating either the period 
// or the test

static void ledtest_update_diff(void) {
    switch (ledtest_test) {
        case ALL_CYCLE:
            diff = (unsigned) (ledtest_period / TICK_PERIOD);
            break;
        case RUNNING_LIGHT:
            diff = (unsigned) (ledtest_period / (2 * NUM_PIXELS * TICK_PERIOD));
            break;
        case OFF:
            // TODO should anything be set differently here?
            break;
    }
}

void ledtest_enable(ledtest_t test) {
    ledtest_test = test;
    ledtest_update_diff();

    // If the test is disabled, clear the LEDs
    if (test == OFF) {
        ledtest_set_all_leds(COL_OFF);
    }

    // Store the tick value for timing reference
    tick_last = tick_value();
}

void ledtest_set_period(float period) {
    ledtest_period = period;
    ledtest_update_diff();
}