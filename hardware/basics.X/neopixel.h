/* 
 * File:   neopixel.h
 * Author: jrs
 *
 * Created on 06 July 2022, 21:56
 */

#ifndef NEOPIXEL_H
#define	NEOPIXEL_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>

    
#define _XTAL_FREQ 32000000   // Fosc (system) lock frequency 
  
#define NUM_PIXELS 8      // Number of neopixels in the strip

// Timings with reference to Fosc = 32MHz
#define FRAME_RATE 60     // Frames per second (Hz)

/// Delay between framebuffer bytes (seconds). This value should be
/// made as high as possible, but not higher than 30us (so as not to come
/// too close to the reset 50us). 
#define BYTE_DELAY 30e-6  
    
/// This is the standard brightness for the predefined colours
#define BRIGHT 5
    
    /**
     * @brief A struct for holding a 24-bit colour code
     */
    typedef struct {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    } colour_t;
    
    extern const colour_t COL_RED;
    extern const colour_t COL_GREEN;
    extern const colour_t COL_BLUE;

    extern const colour_t COL_WHITE;
    extern const colour_t COL_OFF;    
    /**
     * @brief Initialise the neopixel interface
     * 
     * Prerequisites: setup_ws2812()
     */
    void setup_neopxl(void);
    
    /**
     * @brief Set the RGB colour of a neopixel LED
     * 
     * Sets the RGB colour of a particular neopixel. The colour is written
     * into the framebuffer, and the colour is output on the LEDs on the next
     * refresh cycle.
     * 
     * No action is taken if the address is out of range.
     * 
     * @param addr Address of the LED to set, from 0 to (NUM_PIXEL - 1) 
     * @param colour The 24-bit colour code (see the colour_t struct)
     */
    void neopxl_write(unsigned addr, colour_t colour);
    
    /**
     * @brief Send the next byte of the framebuffer
     * 
     * Do not use: this function is intended to be called by the timer0
     * interrupt service routine.
     */
    void neopxl_isr(void);
    

#ifdef	__cplusplus
}
#endif

#endif	/* NEOPIXEL_H */

