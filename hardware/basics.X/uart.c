#include <xc.h>
#include <stdbool.h>

#define _XTAL_FREQ 32000000

/// Set the UART TX receive buffer size here, not in the code below
#define UART_TX_BUFFER_SIZE 16

void setup_uart(void) {
#ifdef PIC16F18346
    // Set the data directions
    TRISBbits.TRISB5 = 0;
    TRISBbits.TRISB6 = 1;

    // Disable analog functions on the pins
    ANSELBbits.ANSB5 = 0;
    ANSELBbits.ANSB6 = 0;

    // Map the output TX pin to RB5
    RB5PPSbits.RB5PPS = 0x14; // TX/CK

    // Map the input RX pin to RB6
    RXPPSbits.RXPPS = 0xe;

    // Set up the baud rate 9600
    SP1BRGL = 51;
    SP1BRGH = 0;

    // Enable the transmitter and receiver 
    TX1STAbits.SYNC = 0; // Asynchronous mode
    TX1STAbits.TXEN = 1; // Transmit enable
    RC1STAbits.SPEN = 1; // Serial port enable
    RC1STAbits.CREN = 1; // Receive enable

    // Enable interrupts
    PIE1bits.RCIE = 1; // Enable receive interrupts

    // Transmitter interrupts are not enabled
    // here; instead, when the first byte is transmitted.

#elif PIC16F18446

    // Code for your PIC here...
    // pg 403 EUSART
    // pg 425 register summary
    // RC1REG
    // TX1REG
    // SP1BRG
    // RC1STA
    // TX1STA
    // BAUD1CON

    // The procedure is as follows:
    // 1). Set the pins we wish to use as digital (not analogue) 
    //      ANSEL register
    // 2). Set the input and output pins 
    //      TRIS register 
    // 3). Enable the transmit register 
    //      TXx register 
    // 4). Enable the receiver register 
    //      RCx
    // 5). Map the transmitter pin
    //
    // 6). Map the receiver pin 
    //
    // 7). Set the Baud rate 
    //      SP1BRGL; SP1BRGH;
    // 8). Enable interrupts
    //      PIE3
    //
    // JS: For the procedure, see also 28.1.1.7 (page 407)

    // pg 230 if a pin has analog functions the ANSEL bit for the pin
    // must be cleared to enable the digital buffer
    // so ANSELB has 7,6,5,4
    // 0 means digital i/o
    ANSELBbits.ANSB5 = 0;
    ANSELBbits.ANSB6 = 0;

    // I can't remember what TRIS A/B/C are again. The data sheet
    // says Register summary input/output
    // blatantly copying 
    // TRISB has bits 7,6,5,4
    TRISBbits.TRISB5 = 0; // B5 output
    TRISBbits.TRISB6 = 1; // B6 input

    // pg. 233 PPS Outputs
    // each I/O pin has a pps register 
    // the pin registers look like Rxy, where x is the PORT letter and y is the bit
    // number
    // Map the output TX pin to RB5
    // e.g. RA0 for the RA0PPS register.

    // input signal name| input reg name| default loc   | reset val
    // --------------------------------------------------------------
    //      RX1 / DT1   |    RX1PPS     |     RB5       |   01101                 |               |               |
    //      TX1 / CK1   |    CK1PPS     |     RB7       |   01111 

    // RX1PPS
    // I have a horible feeling that the CK1 is going to be an issue
    // CK1PPSbits.
    // JS: I think you can probable just ignore CK1. It is only used
    //     when the EUSART peripheral is in sycnhronous mode (we are
    //     using async mode, no clock)

    // Map the output TX pin to RB5
    // p.g. 231 RX1/DT1 RX1PPS 01101 
    // RB5PPSbits.RB5PPS = 0x14; // TX/CK
    // // Map the input RX pin to RB6
    // RXPPSbits.RXPPS = 0xe;

    // pps - peripheral pin select
    // pin output source 
    // output peripheral -> to pin
    //
    // each pin has a register for periperal output 
    // RB5PPSbit = TX1 is uart is specific 
    // pg 234
    //
    // pg 404 is eusart transmit
    // output direction 
    // writing number into pin reg
    // RxyPPS = number 
    //
    // input where RX corresponds to the recevier of the uart
    // RXPPS = number 
    
    // PIC   RB5 (Tx) ----- (Rx) GPIO15 (10)   Raspberry PI 
    //      RB6 (Rx) ----- (Tx) GPIO14 (8)
    // Peripheral pin select module p.g. 233
    RB5PPSbits.RB5PPS = 0xF;
    
    // input 
    // map the uart rtx to RB6
    // pg 239 or 232 
    RX1PPSbits.RX1DTPPS = 0xE;

    // Maybe John could explain this to me.
    // JS: See table 28-2 on page 412. The values in these registers
    //     make some kind of period that is used to generate the
    //     timing, but I didn't do any calculations (just took the
    //     numbers). The L and H registers are concatenated to make
    //     a 16 bit value, but only 8 bits are used here
    // Set up the baud rate 9600
    SP1BRGL = 51;
    SP1BRGH = 0;

     // no idea what TX is...
    // It is the Transmit data register
    // so I know there is a TX1STA register 
    // when you go to pg 430 the transmit status and control register
    // is TXxSTA and I think there is only 1 (one) 
    TX1STAbits.SYNC = 0;
    TX1STAbits.TXEN = 1;

    // Receive register 
    RC1STAbits.SPEN = 1; // enable serial port
    RC1STAbits.CREN = 1; // depends on SYNC? Either enables receiver 
    // or continuous receiver

    // Enable interrupts
    // pg 106 
    // JS: I just changed the bit name to RC1IE to make it compile
    //     (see also the names in the interrupts.c file, isr() function).
    //     Also, you could comment out this RX interrupt enable to start
    //     with just to test the TX direction -- it doesn't affect that.
    PIE3bits.RC1IE = 1; // Enable receive interrupts

#endif

    // Short delay seems to be necessary not to confuse the
    // pi with the first transmission
    __delay_ms(1);
}

/**
 * @brief Send data via uart (blocking)
 * @param data The character to send via uart
 * 
 * Prerequisites: setup_uart()
 * 
 * The function will send data as soon as the transmit
 * buffer is available, and then return immediately.
 */
void uart_blocking_write(char data) {
    while (TX1STAbits.TRMT == 0)
        ; // Wait for transmit buffer to be empty

    // Transmit the data
    TX1REG = data;
}

/**
 * @brief A transmit buffer for UART characters
 */
static char tx_buffer[UART_TX_BUFFER_SIZE];

/**
 * @brief A structure for holding the next available TX buffer entry
 */
static struct {
    char * read;
    char * write;
    bool full;
    bool empty;
} tx_ptr = {.read = tx_buffer, .write = tx_buffer,
    .full = false, .empty = true};

/// Push to TX buffer from user application

void uart_write(char data) {

    while(tx_ptr.full)
        ; // Wait for space in the buffer to become available
    
    // Store the data
    *tx_ptr.write = data;
    
    // Update the write pointer
    tx_ptr.write++;
    if (tx_ptr.write == tx_buffer + UART_TX_BUFFER_SIZE) {
        tx_ptr.write = tx_buffer;
    }
    
    // Mark the buffer as full if necessary
    if (tx_ptr.write == tx_ptr.read) {
        tx_ptr.full = true;
    }
    
    // Mark the buffer as not empty
    tx_ptr.empty = false;

    // Enable the transmit interrupt so as to initiate
    // sending data. This will immediately cause an interrupt
    // that will send the byte out from the transmit buffer.
    // This interrupt is only disabled from within the uart_pop_tx
    // function, when there is no more data in the buffer.
#ifdef PIC16F18346
    PIE1bits.TXIE = 1;
#elif PIC16F18446
    PIE3bits.TX1IE = 1;
#endif
}
/// Pop from TX buffer in ISR and send out to UART TX pin

void uart_pop_tx(void) {

    // Return immediately if the buffer is empty. This is
    // probably not necessary, but for some reason I am 
    // getting TX interrupt flags causing calls to this
    // function in the isr. TODO To fix later.
    if (tx_ptr.empty == true) {
        return;
    }
    
    // Pop data from the buffer into the transmit
    // buffer register
    TX1REG = *tx_ptr.read;

    // Update the read pointer
    tx_ptr.read++;
    if (tx_ptr.read == tx_buffer + UART_TX_BUFFER_SIZE) {
        tx_ptr.read = tx_buffer;
    }

    // Mark the buffer as not full. It is quicker to
    // just clear the flag than to check if it needs
    // clearing and then clear it.
    tx_ptr.full = false;

    // If there is no more data to send, disable the transmit
    // interrupt
    if (tx_ptr.read == tx_ptr.write) {
#ifdef PIC16F18346
        PIE1bits.TXIE = 0;
#elif PIC16F18446
        PIE3bits.TX1IE = 0;
#endif
        // Mark the buffer as empty
        tx_ptr.empty = true;
    }

}

