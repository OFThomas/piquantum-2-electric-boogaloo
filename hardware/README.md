# Hardware information

This is the hardware folder. Plans, specifications, designs and microcontroller code goes here.

## Option 1: Reconfigurable optical-element boards

The idea is to have an array of PCBs (about 10cmx10cm), each of which has a microcontroller and a largish number of RGB LEDs (about 80-100 say), which are shaped in such a way that they can realise waveguides, MZIs, detectors, modulators, etc. Each board should be the same design, to maximise cost-efficiency of the PCB manufacture, and the complete set of boards should be controlled by one Raspberry Pi, which hands the simulation. The microcontrollers control the LEDs and communicate with each other and the Raspberry Pi.

The spreadsheet [](option1.ods)

### Power calculations

**Summary: Maximum sensible supply: 100W. Capable of driver 375 LEDs at full brightness, or 750 at half brightness.**

The power consumption of the project depends primarily of the power consumption of the LEDs. The neopixel (SK6812) consume a baseline 1mA (when off, see the datasheet), and draw up to 60mA at full brightness (from [here](https://www.digikey.co.uk/en/maker/projects/sipping-power-with-neopixels/aaefcc52e9a74b7dbb8bdfb5c522bcdc)), or 0.2W (implying 40mA from the datasheet). The supply voltage is 5V.

Assuming a total power consumption of 50W (2.55A at 20V, so about the normal power consumption of a laptop), you could run 250 neopixels at full brightness. At half brightness, 500 pixels would be possible. However, it would be better to reduce the power consumption of the project and halve the brightness and the power supply.

A sensible maximum power consumption might be 75W, provided by a power supply capable of about 100W, allowing 25% headroom vs. the maximum rating. This works out as 375 LEDs at maximum brightness, or 750 at half brightness. It might be possible to have more LEDs if there was a mechanism to ensure that only a subset were turned on at once. That would increase hardware complexity slightly. Also, it may be undesirable not to be able to turn all LEDs on at once. Any circuit should also have a hardware current limiter to ensure that a predefined upper limit is not exceeded. At this power (and providing that power is distributed between the boards using a shared power rail), each board may have to cope with up to 15A on its main power rail (in particular, the board which is the entry point for the power supply). 

### Adding an external power supply to the curiosity boards

All part numbers referred to below are from Farnell, on 28/06/2022.

The voltage regulator TP3 is not populated. The recommended regulator is the LM340, however this is not easily available to buy anymore. Instead, we will use the TO-220 version of the package: LM340T-5.0/NOPB.

The power adapter is a 2.5mm barrel plug. The 694108106102 should be suitable.




