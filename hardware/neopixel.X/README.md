# Neopixel test using PIC16F

The object of this test is to establish whether or not the PIC16F can drive neopixel LEDs, and what power the LEDs consume.

The test uses a strip of 8 WS2812 LEDs. The timing protocol used by this driver is as follows:
- 0: 350ns high, then 800ns low (total 1150ns)
- 1: 700ns high, then 600ns low (total 1500ns)

All times (high and low intervals) have a tolerance of +-150ns. In the application note about generating this timing protocol using the PIC16F CLC, the 1 bits were generated using one half period of the clock, and the 0 bits were generated using the PWM module (to set a smaller duty cycle). It therefore seems that both bits can be made to use the same period (this isn't very clear to me from the WS2812 datasheet). The datasheet does state TH+TL=1.25us, but since neither of the bit times add up to this number, it is not clear how to interpret.

In practice, it seems quite likely that there are two parameters to be tuned:
- The overall clock rate, which determines the bit rate (the same for both bits). Half of this clock rate is used as the high time for the 1 bit, so this would need to respect the (minimum?) 700ns stated above.
- The PWM duty cycle, selected so that a pulse of the PWM meets the 0 bit high time 350 ns stated above.

## Development board

This test uses the Microchip curiosity low pin count development board, and the PIC16F18346. On that board, the following pins seem to be available for use (not connected to on-board LEDs, buttons, etc.): RA4, RC3, RC6, RC7, RB7, RB6, RB5, RB4, RC2. (Some of these pins are connected to the microbus, which is not being used here.)

## Peripherals

The neopixel driver uses a custom peripheral created using the CLC (configurable logic cell), combining a serial data output, a clock, and a PWM output. This section contains notes about getting these peripherals working individually first.

### PWM 

There are two PWM modules, labelled PWM5 and PWM6. PWM5 is used here. The PWM module is not mapped to any particular pin by default. For testing, the peripheral is mapped to RA4 by setting RA4PPS to 2.

### MSSP as SPI

To set up the MSSP for SPI, set the SDA and SCK outputs to RC3 and RC6 by writing 0x19 to RC3PPS and 0x18 to RC6PPS. Then write 0x23 to SSP1CON1 to configure and enable the SPI master. To send a byte, write to the SSP1BUF register.
 
### CLC

The goal is to realise the following logic function:

OUT = SCL x (SDO + PWM)

which may be interpreted as follows: OUT is only ever high on the high half-cycle of the clock SCL; and in that case, if SDO is high, then OUT is high for the half the clock period (corresponding to 1); else, it is only high for the PWM on phase (corresponding to 0).

Peripheral is mapped to pin RC7 by writing 0x4 to RC7PPS and setting the TRIS bit to 0.

## Timings

Using the SPI/PWM scheme above, the following timings are obtained using the internal 32MHz oscillator:
- Logic "0" pulse = 384ns high, then 976ns low (total 1360ns)
- Logic "1" pulse = 748ns high, then 880ns low (total 1628ns)

(Note from later: the numbers shouldn't add up to different totals, not by this much -- since the total period is defined by T2, with the same frequency for both bits)

The settings are T2CON = 0x04, PR2 = 5, PWM5DCH = 3, PWM5DCL = 0, SSP1CON1 = 0x23, plus the correct configuration of the CLC peripheral to realise the logic function above. 

## Power consumption

This first test is a rough measurement based on a bench power supply current reading (resolution of 10mA). All LEDs (8) were set to the brightness shown, for each colour seperately and then all colours combined (white):
- Brightness 255: green = 100mA, red = 100mA, blue = 100mA, white = 290mA.
- Brightness 200: green = 70mA, red = 70mA, blue = 70mA, white = 230mA.
- Brightness 150: green = 60mA, red = 60mA, blue = 60mA, white = 170mA.
- Brightness 100: green = 40mA, red = 40mA, blue = 40mA, white = 110mA.
- Brightness 50: green = 20mA, red = 20mA, blue = 20mA, white = 60mA. Notes: R ok, g/b/w still too bright.
- Brightness 30: green = 10mA, red = 10mA, blue = 10mA, white = 30mA. Notes: w a bit better, b/g/r OK to look at.

These measurements show that a full white LED at a reasonable brightness (about 30) consumes approximately 4mA, an order of magnitude less than the 40mA consumption at max brightness. This corresponds to 0.02W. So, for a power budget of 10W, it would be possible to light 500 LEDs

These are the current measurements using a multimeter:

- 0 LED (baseline current consumption of 8 LEDs): 2.2mA (so 0.275mA per LED) 
- 1 LED, green, brightness = 255: 14.3mA (12.4mA after correction)
- 1 LED, red, brightness = 255: 14.2mA (12.3mA after correction)
- 1 LED, blue, brightness = 255: 14.2mA  (12.3mA after correction)
- 8 LED, green, brightness = 255: 98.5mA (=> each LED 12.31mA) 
- 8 LED, red, brightness = 255: 98.3mA (=> each LED 12.29mA) 
- 8 LED, blue, brightness = 255: 98.8mA (=> each LED 12.35mA) 

At a high brightness:

- 1 LED, green, brightness = 100: 6.9mA (5.0mA after correction)
- 1 LED, red, brightness = 100: 6.9mA (5.0mA after correction)
- 1 LED, blue, brightness = 100: 6.9mA  (5.0mA after correction)
- 8 LED, green, brightness = 100: 39.9mA (=> each LED 4.99mA) 
- 8 LED, red, brightn+ess = 100: 39.8mA (=> each LED 4.98mA) 
- 8 LED, blue, brightness = 100: 40.0mA (=> each LED 5.0mA) 

At the maximum reasonable brightness (for viewing)

- 8 LED, green, brightness = 50: 21.0mA (=> each LED 2.63mA) 
- 8 LED, red, brightness = 50: 20.9mA (=> each LED 2.61mA) 
- 8 LED, blue, brightness = 50: 21.1mA (=> each LED 2.64mA) 

At a low brightness level (note that the current is low enough for the baseline current to matter)

- 8 LED, green, brightness = 5: 3.9mA (=> each LED 0.49mA) 
- 8 LED, red, brightness = 5: 3.9mA (=> each LED 0.49mA) 
- 8 LED, blue, brightness = 5: 4.0mA (=> each LED 0.50mA) 

From these numbers, each LED consumes the same current, and each colour also consumes the same current. Doing a simple calculation about the gradient of the current with brightness shows that it is linear with approximately the same gradient in the range 255-100 and 50-5:

(1/8)*(98.8-3.9)/(255-5) = 0.047mA/brightness level,

for each LED. So an approximate formula for the current consumption of an LED is:

**current/Amps = 4.7e-5*(r + g + b) + 2.75e-4**,

where r, g and b are the brightess levels (up to 255). For example, if r=100, g=b=0, then current = 4.9mA, which roughly agrees with the above. At the maximum brightness r=g=b=50, current = 7.3mA (for one LED), corresponding to 36mW. 
