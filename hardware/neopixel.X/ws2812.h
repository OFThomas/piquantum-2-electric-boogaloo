/* 
 * File:   ws2812.h
 * Author: jrs
 *
 * Created on 14 June 2022, 16:09
 */

#ifndef WS2812_H
#define	WS2812_H

/**
 * \brief Initialise the WS2812 driver
 *
 */
void initialise_ws2812(void);

#endif	/* WS2812_H */

