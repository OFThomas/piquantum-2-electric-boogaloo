#include <xc.h>
#include "io.h"


void __interrupt() isr(void)
{
    /*
    if (INTCONbits.TMR0IF == 1) {
        //toggleD4();
        LATAbits.LATA5 = ~LATAbits.LATA5;
        
        if (PORTAbits.RA5 == 0) {
            TMR0bits.TMR0 = 0;
        }
        else {
            TMR0bits.TMR0 = 255; 
        }
        
        INTCONbits.TMR0IF = 0;
    }
    */
    if (PIR1bits.TMR1IF == 1) {

        //toggle_d4();
        //LATAbits.LATA5 = ~LATAbits.LATA5;
        /*
        if (LATAbits.LATA5 == 0) {
            // off time
            TMR1Lbits.TMR1L = 0x24;
            TMR1Hbits.TMR1H = 0xf4;
        }
        else {
            // on time
            TMR1Lbits.TMR1L = 0xfe;
            TMR1Hbits.TMR1H = 0xff; 
        }
        */
        
        /*
        // Start analog to digital conversion
        ADCON0bits.GO = 1;
        // Wait until conversion done;
        while (ADCON0bits.GO == 1);
        // Read into timer
        TMR1Lbits.TMR1L = ADRESL;
        TMR1Hbits.TMR1H = ADRESH;
        */
        
        PIR1bits.TMR1IF = 0;
    }
    return;
}
