#include <xc.h>

void initialise_ws2812(void) {


    // Initialise PWM5
    T2CON = 0x04;
    PR2 = 5;
    
    // Set the PWM to active low, and 
    PWM5CON = 0x80;
    
    // Set the duty cycle
    PWM5DCH = 0x3;
    PWM5DCL = 0x0;
    
    
    // Map the PWM to an output
    RA4PPS = 2; // Set RA4 to output PWM5
    TRISAbits.TRISA4 = 0;
    
    // Set up serial peripheral
    RC3PPS = 0x19; // Map the data output of the SPI to RC3
    TRISCbits.TRISC3 = 0;
    RC6PPS = 0x18; // Map the clock output of the SPI to RC3=6
    TRISCbits.TRISC6 = 0;
    
    SSP1CON1 = 0x23;
    
    // Set up the CLC to perform the operation: 
    // OUT = SCL x (SDA + PWM)
    
    // Set up the (three) inputs to the CLC
    CLC1SEL0 = 18; // D1 = SCL1 (the clock line) 
    CLC1SEL1 = 19; // D2 = SDA1 (the data line) 
    CLC1SEL2 = 16; // D3 = PWM5 output
    CLC1SEL3 = 0;
    
    // Set up the data gating stage. Note that there is a typo in the
    // datasheet -- the gate stage implements a OR gate when all inputs
    // and ouputs are non-inverted, not an AND gate. 
    CLC1GLS0 = 0x02; // lc1g1 = D1
    CLC1GLS1 = 0x28; // lc1g2 = D2 + D3
    CLC1GLS2 = 0; // lc1g3 = 1 (so that AND will work)
    CLC1GLS3 = 0; // lc1g4 = 1
    CLC1POL = 0xc; // This sets the output negation used in lc1g2,3,4
    
    // Set up the peripheral pin mapping
    RC7PPS = 0x4;
    TRISCbits.TRISC7 = 0;
    
    // Set up the logic function: perform logical 
    // AND between the gate outputs lc1gN
    CLC1CON = 0x82;
    
    
}
