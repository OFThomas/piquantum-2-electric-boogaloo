/**
 * \file io.c
 * \brief Contains input/output functions for the LEDs and buttons
 * 
 * Call initialiseIO(void) at the start of the main program to use the LEDs and
 * buttons. 
 */

#include "io.h"
#include <xc.h>

/**
 * \brief Set up the LEDS and buttons
 * 
 * This function sets up the I/O ports so that the four LEDs are outputs and
 * the button is connected to an input. 
 * 
 */
void initialise_io(void) {
    
    // Set up LEDs - configure pins as outputs
    TRISAbits.TRISA5 = 0;   // D4
    TRISAbits.TRISA1 = 0;   // D5
    TRISAbits.TRISA2 = 0;   // D6
    TRISCbits.TRISC5 = 0;   // D7
    
    // Initialise all the LEDs to off
    LATA = 0;
    LATC = 0;
    
    // Set up button S1
    TRISCbits.TRISC4 = 1;  // Configure pin as input
    ANSELCbits.ANSC4 = 0;  // Set to digital input
    
    // Initialise potentiometer
    TRISCbits.TRISC0 = 1;   // Configure pin as input
    ANSELCbits.ANSC0 = 1;   // Configure pin as analog
    ADCON1bits.ADCS = 2;    // Select conversion clock, FOSC/32 (for 32MHz)
    ADCON1bits.ADPREF = 0;  // Configure voltage reference, VDD
    ADCON0bits.CHS = 0x10;     // Select input channel, ANC0
    ADCON0bits.ADON = 1;    // Turn on ADC module
    ADCON1bits.ADFM = 1;    // Right justify result
    

}


/*********************************************************************/
void set_d4(bool state)
{
    if (state) {
        // Turn on the LED
        LATAbits.LATA5 = 1;
    }
    else {
        // Turn off the LED
        LATAbits.LATA5 = 0;
    }
}


/*********************************************************************/
void set_d5(bool state)
{
    if (state) {
        // Turn on the LED
        LATAbits.LATA1 = 1;
    }
    else {
        // Turn off the LED
        LATAbits.LATA1 = 0;
    }
}


/*********************************************************************/
void set_d6(bool state)
{
    if (state) {
        // Turn on the LED
        LATAbits.LATA2 = 1;
    }
    else {
        // Turn off the LED
        LATAbits.LATA2 = 0;
    }
}


/*********************************************************************/
void set_d7(bool state)
{
    if (state) {
        // Turn on the LED
        LATCbits.LATC5 = 1;
    }
    else {
        // Turn off the LED
        LATCbits.LATC5 = 0;
    }
}


/*********************************************************************/
bool is_button_pressed(void)
{
    // Returns true if the button is pressed
    if (PORTCbits.RC4 == 0) {
        return true;
    }
    else {
        return false;
    }
}

/*********************************************************************/
static bool button_state = false;

/*********************************************************************/
bool get_button_state(void) {
    if(button_state == true) {
        button_state = false; // Reset state
        return true;
    } else {
        return false;
    }
}

/*********************************************************************/
void wait_for_button(void)
{
    // Wait for previous button to be released
    while (is_button_pressed() == true);
    // Wait for current button press
    while (is_button_pressed() == false);
    
    // Store the button press
    button_state = true;
    
    return;
}


/*********************************************************************/
void display_number(uint8_t n) 
{
    // Return if the number is bigger than 15
    // TO DO: add some kind of error alert e.g. flashing lights
    if (n > 16) 
        return;
    
    clear_leds();
    // Turn on the relevant lights
    // D4 is the least significant bit    
    if ((n & 1) == 1) {
        set_d4(true);
    }
    if ((n & 2) == 2) {
        set_d5(true);
    }
    if ((n & 4) == 4) {
        set_d6(true);
    }
    if ((n & 8) == 8) {
        set_d7(true);
    }
}


/*********************************************************************/
void clear_leds(void)
{
    set_d4(false);
    set_d5(false);
    set_d6(false);
    set_d7(false);
}

/*********************************************************************/
/**
 * \brief Get the 10-bit potentiometer value, between 0 and 1
 * 
 */
float get_pot_value(void)
{
    // Start analog to digital conversion
    ADCON0bits.GO = 1;
    // Wait until conversion done;
    while (ADCON0bits.GO == 1);
    
    uint16_t value = ((uint16_t)ADRESH << 8) + ADRESL;  // Right justified result
    //uint16_t value = (ADRESL >> 6) + (ADRESH << 2);

    return ((float)value)/0x3ff;
}

// ADRESH ADRESL