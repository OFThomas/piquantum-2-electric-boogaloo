/**
 * \file io.h
 * \brief Contains functions for writing to LEDs and reading buttons
 */

#ifndef IO_H
#define	IO_H

#include <stdbool.h>
#include <stdint.h>

/**
 * \brief Set up the LEDS and buttons
 * 
 * This function sets up the I/O ports so that the four LEDs are outputs and
 * the button is connected to an input. 
 * 
 */
void initialise_io(void);

/// Set LED D4 to on/off
void set_d4(bool state);

/// Set LED D4 to on/off
void set_d5(bool state);

/// Set LED D4 to on/off
void set_d6(bool state);

/// Set LED D4 to on/off
void set_d7(bool state);

/// Switch LED D4 to on if it's initially off and vice-versa
void toggle_d4(void);

/// Switch LED D5 to on if it's initially off and vice-versa
void toggle_d5(void);

/// Get current state of button (pushed down or up)
bool is_button_pressed(void);

/// Returns once the user has pressed a button
void wait_for_button(void);

/// Check whether a button has been pressed
bool get_button_state(void);

/// Display an integer on the LEDs in binary
void display_number(uint8_t n);

/// Clear all the LEDs
void clear_leds(void);

// Get the 10-bit potentiometer output, as a value between 0 and 1
float get_pot_value(void);

#endif
