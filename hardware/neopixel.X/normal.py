from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-128,128,257)
y = norm.pdf(x,loc=0,scale=16)
y = [v/y[128] for v in y]

for n in range(len(y)):
    print(f"{y[n]:.3f}", end=",")
    if ((n+1) % 8 == 0):
        print("")
    

plt.show()
