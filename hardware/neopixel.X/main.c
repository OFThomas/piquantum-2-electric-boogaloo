/*
 * File:   main.c
 * Author: jrs
 *
 * Created on 14 June 2022, 14:29
 */

// Fosc = 32MHz
#define _XTAL_FREQ 32000000

#include <xc.h>

#include "io.h"
#include "ws2812.h"

#pragma config WDTE = OFF
#pragma config RSTOSC = HFINT32 // Use the 32MHz internal oscillator
#pragma config FEXTOSC = OFF // Otherwise RA5 won't work

static uint8_t colours[24] = {
    0,0,5,
    0,0,5,
    0,0,5,
    0,0,5,
    0,0,5,
    0,0,5,
    0,0,5,
    0,0,5
};

/// LED positions
const static int x_leds[8] = {
    0,16,32,48,64,96,112,128
};

const static float y_norm[257] = {
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.001, 0.001, 0.001, 0.001, 0.001, 0.002,
    0.002, 0.003, 0.003, 0.004, 0.005, 0.006, 0.008, 0.009,
    0.011, 0.013, 0.016, 0.019, 0.023, 0.027, 0.032, 0.038,
    0.044, 0.051, 0.060, 0.069, 0.080, 0.091, 0.105, 0.119,
    0.135, 0.153, 0.172, 0.193, 0.216, 0.241, 0.267, 0.295,
    0.325, 0.356, 0.389, 0.423, 0.458, 0.494, 0.531, 0.569,
    0.607, 0.644, 0.682, 0.719, 0.755, 0.790, 0.823, 0.854,
    0.882, 0.909, 0.932, 0.952, 0.969, 0.983, 0.992, 0.998,
    1.000, 0.998, 0.992, 0.983, 0.969, 0.952, 0.932, 0.909,
    0.882, 0.854, 0.823, 0.790, 0.755, 0.719, 0.682, 0.644,
    0.607, 0.569, 0.531, 0.494, 0.458, 0.423, 0.389, 0.356,
    0.325, 0.295, 0.267, 0.241, 0.216, 0.193, 0.172, 0.153,
    0.135, 0.119, 0.105, 0.091, 0.080, 0.069, 0.060, 0.051,
    0.044, 0.038, 0.032, 0.027, 0.023, 0.019, 0.016, 0.013,
    0.011, 0.009, 0.008, 0.006, 0.005, 0.004, 0.003, 0.003,
    0.002, 0.002, 0.001, 0.001, 0.001, 0.001, 0.001, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000,
    0.000
};

const static int x_off = 128;

#define GREEN 0
#define RED 1
# define BLUE 2

/**
 * Set the colour 
 * 
 * 
 * @param x_c
 * @param x_led
 */
void set_colours(int x_c, int col) {
    
    // Loop over all the LEDs setting their brightness according
    // to the position of the normal distribution (given by x_c).
    for (int n = 0; n < 8; n++) {
        int x_led = x_leds[n];
        colours[3*n+col] = (uint8_t)(30 * y_norm[x_led - x_c + x_off]);
    }
}


/**
 * \brief Perform basic device setup (oscillators, etc.)
 * 
 */
void initialise_basic(void)
{
    // Assume that RSTOSC is set to HFINT32
   
    // Set the postscalar to 1 to create Fosc = 32MHz
    OSCCON1bits.NDIV = 0;
}

void main(void) {
    
    initialise_basic();
    
    initialise_io();
    
    //display_number(0xf);
    
    initialise_ws2812();
    
    // Enable interrupts
    //INTCONbits.GIE = 1;

    /*
    const uint8_t brightness = 30;
    // Set all the LEDs to GREEN
    for (int n = 0; n < 8; n++) {
        colours[3 * n + RED] = brightness;
        //colours[3 * n + GREEN] = brightness;
        //colours[3 * n + BLUE] = brightness;
    }
     * */

    /**/
    // Write colours
    for (int n = 0; n < 24; n++) {
        SSP1BUF = colours[n];
        __delay_us(20);

    }
    
    // Power consumption test
    while(true);
    
    // Normal distribution running lights
    while(true) {
        
        
        
        // Compute the center position of the normal distribution
        // based on the potentiometer setting
        float value = get_pot_value(); // 0 to 1
        uint8_t green_x_c = (uint8_t)(128 * value); 
        uint8_t red_x_c = (uint8_t)(128 * (1-value));
        
        // Calculate the colours
        set_colours(green_x_c, GREEN);
        set_colours(red_x_c, RED);
        
        for (int n = 0; n < 24; n++) {
            SSP1BUF = colours[n];
            __delay_us(20);
        
        }
        __delay_us(1000);
    }

}
