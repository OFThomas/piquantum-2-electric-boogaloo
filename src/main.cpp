/**
 * @author      : oliver (oliver@oliver-pc)
 * @file        : main
 * @created     : Friday Jun 10, 2022 18:53:55 BST
 */

#include <iostream>
#include <string>
#include <vector>

#include <variant> // std::variant for the shapes 

#include <olistd/io>
#include <olistd/vector>
#include <olistd/timer>
#include <olistd/maths>
#include <olistd/random>

#include "drawing.hpp"
#include "optics.hpp"
#include "maths.hpp"

// Contains the number of rows and columns we have 
// template <typename data_t, template<typename> class Buffer>
template <typename buffer_t>
class Chip
{
    private:
        size_t num_rows;
        size_t num_cols;
    public:
        std::vector<buffer_t> waveguides;

        Chip(size_t num_rows, size_t num_cols) : num_rows(num_rows), num_cols(num_cols), waveguides(num_rows, buffer_t{num_cols})
        {

        }
};


int main(int argc, char * argv[])
{

    int choice = 0;
    if(argc > 1)
        choice = std::stoi(argv[1]);

    // std::cout << "#Running case " << choice << std::endl;

    switch(choice)
    {
        case 0:
            {
                PiQ::maths::benchmarks();
                break;
            }
        case 1:
            {
                // vector of waveguides (each waveguide is a row of leds)
                // lets make a 16 element one 
                PiQ::optics::Waveguide<size_t> waveguide{16};

                std::vector<size_t> idx_to_set = {2, 4, 6, 7};
                std::vector<size_t> val_to_set = {1, 4, 2, 3};
                for(size_t i = 0; i < idx_to_set.size(); i++)
                {
                    waveguide.set(idx_to_set[i], val_to_set[i]);
                }
                // should print  
                // 0, 0, 1, 1, 4, 4, 2, 3, ... , 3
                olistd::print(waveguide.write());

                // rows, cols
                Chip<PiQ::optics::Waveguide<int>> chip{4, 8};
                for(PiQ::optics::Waveguide<int> & waveguide : chip.waveguides)
                {
                    std::vector<size_t> idx = olistd::random_vector_sorted<size_t>(8, 4);
                    std::vector<int> vals = olistd::random_vector<int>(4, 3);
                    olistd::print(idx);
                    olistd::print(vals);

                    waveguide.set(idx, vals);
                    olistd::print(waveguide.write());
                }

                break;
            }
        case 2:
            {
                // grid.write("plot.dat");

                size_t resolution = 16;
                if(argc > 2)
                    resolution = std::stoi(argv[2]);
                // std::vector<float> w = olistd::lin_vec<float>(0.0, 1.0, resolution);
                // for(float v1 : w)
                //     for(float v2 : w)
                //         std::cout << v1 << " " << v2 << " " << line(v1, v2) << std::endl;

                PiQ::Draw::LED_grid grid{resolution, resolution};
                PiQ::Draw::Line line({0,0.5}, {1,0.5});
                PiQ::Draw::Circle circle({0.5,0.5}, 0.4, 0.2);
                // for(float v1 : w)
                //     for(float v2 : w)
                //         std::cout << v1 << " " << v2 << " " << circle(v1, v2) << std::endl;
                grid.add(line);
                grid.add(circle);
                grid.add(PiQ::Draw::Circle{{0.5,0.5}, 0.4, 0.2});
                // grid.draw();
                grid.write_rgb();

                PiQ::Draw::LED_hex hex_grid{resolution, resolution};
                hex_grid.add(line);
                hex_grid.add(circle);
                hex_grid.add(PiQ::Draw::Circle{{0.5, 0.5}, 0.4, 0.2});
                // hex_grid.draw();


                break;
            }
        case 3:
            {
                // neopixel grid 256 leds.
                PiQ::Draw::LED_grid grid{16,16};

                // grid.add(PiQ::Draw::Line{{0,0.5},{1,0.5}});
                PiQ::Draw::Waveguide waveguide{{0, 0.5}, 1, 0.05};
                grid.add(waveguide);

                size_t num_test_points = 20;
                std::vector<PiQ::Draw::Point> points(num_test_points);
                for(PiQ::Draw::Point & point : points)
                {
                    point.x = olistd::random_number(0.0, 1.0);
                    point.y = olistd::random_number(0.0, 1.0);

                    // waveguide.bbox(point);


                    //                     std::cout << point;
                    //                     if(waveguide.bbox(point))
                    //                         std::cout << " inside" << std::endl;
                    //                     else 
                    //                         std::cout << " outside" << std::endl;
                }

                grid.write("test.dat");
                // grid.draw();

                PiQ::Draw::LED_hex hex_grid{16,16, "hex.dat"};
                hex_grid.add(waveguide);
                hex_grid.write();



                break;
            }
        case 4:
            {
                size_t dim = 16;
                size_t half = dim/2;
                PiQ::Draw::LED_grid grid{dim, dim, "grid.dat0"};
                grid.add(PiQ::Draw::Waveguide{{0,half}, dim, 0});
                grid.write();
                grid.set(PiQ::Draw::Circle{{half,half}, 6, 0.5});
                grid.write();
                // PiQ::Draw::LED_hex hex_grid{dim, dim, "hex.dat"};
                // hex_grid.set(PiQ::Draw::Waveguide{});
                break;
            }
    }

    return 0;
}



