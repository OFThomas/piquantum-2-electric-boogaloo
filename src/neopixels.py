# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

# Simple test for NeoPixels on Raspberry Pi
import time
import board
import neopixel

import fileinput
from datetime import datetime

# Choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D18
# NeoPixels must be connected to D10, D12, D18 or D21 to work.
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 256

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

max_brightness = 0.005
# max_brightness = 0.01

pixels = neopixel.NeoPixel(
        pixel_pin, num_pixels, brightness=max_brightness, auto_write=False, pixel_order=ORDER
        )
def reset():
    for i in range(num_pixels):
        pixels[i] = (0,0,0)

def line(delay):
    counter = 0
    while True:
        for i in range(num_pixels):
            pixels[i] = (0,0,0)
            if i == counter:
                pixels[i] = (255, 0,0)

        pixels.show()
        time.sleep(delay)
        counter += 1
        if counter == num_pixels:
            counter = 0


def read_from_std_in(delay):

    counter = 0
    for line in fileinput.input():
        # print(line)
        rgb = tuple(map(int, line.split()))
        # print(rgb)
        pixels[counter] = rgb
        counter += 1
        if counter == num_pixels:
            counter = 0
            pixels.show()
            time.sleep(delay)

    pixels.show()
    time.sleep(1)


def digit(pixels, number, start_row, start_col):
    stride = 16
    flip = stride - 1
    off = (0,0,0)
    for j in range(5):
        pixels[(start_col + 0) * stride + start_row + j] = off
        pixels[(start_col + 1) * stride + flip - (start_row + j)] = off
        pixels[(start_col + 2) * stride + start_row + j] = off
    colour = (255,0,0)
    if number == 0 or number == "0":
        """
        ### 
        # #
        # #
        # #
        ###
        """

        for i in range(5):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(0, 5, 4):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(5):
            pixels[(start_col + 2) * stride + start_row + i] = colour

    elif number == 1 or number == "1":
        """
        ##  
         #
         #
         #
        ###
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(5):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        pixels[(start_col + 2) * stride + start_row + 4] = colour

    elif number == 2 or number == "2":
        """
        ##
          #
         #
        # 
        ###
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + start_row + i] = colour
        pixels[start_col * stride + start_row + 3] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(1, 5, 3):
            pixels[(start_col + 2) * stride + start_row + i] = colour

    elif number == 3 or number == "3":
        """
        ###
          #
         #
          #
        ###
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + start_row + i] = colour
        pixels[(start_col + 2) * stride + start_row + 2] = off

    elif number == 4 or number == "4":
        """
        # #
        # #
        ###
          #
          #
        """
        for i in range(0, 3, 1):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(2, 3, 1):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + start_row + i] = colour


    elif number == 5 or number == "5":
        """
        ###
        #
        ##
          #
        ##  
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + start_row + i] = colour
        pixels[start_col * stride + start_row + 3] = off

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 4, 3):
            pixels[(start_col + 2) * stride + start_row + i] = colour

    elif number == 6 or number == "6":
        """
        ###
        #
        ###
        # #
        ###
        """

        for i in range(0, 5, 1):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + start_row + i] = colour
        pixels[(start_col + 2) * stride + start_row + 1] = off

    elif number == 7 or number == "7":
        """
        ###
          #
         #
        #
        #
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + start_row + i] = colour
        pixels[start_col * stride + start_row + 1] = off
        pixels[start_col * stride + start_row + 2] = off

        for i in range(0, 3, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 1, 1):
            pixels[(start_col + 2) * stride + start_row + i] = colour

    elif number == 8 or number == "8":
        """
        ###
        # #
        ###
        # #
        ###
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + start_row + i] = colour

    elif number == 9 or number == "9":
        """
        ###
        # #
        ###
          #
          #
        """
        for i in range(0, 3, 1):
            pixels[start_col * stride + start_row + i] = colour

        for i in range(0, 3, 2):
            pixels[(start_col + 1) * stride + flip -(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + start_row + i] = colour


    elif number == ":":
        """
         #   

         #

        """
        pixels[(start_col) * stride + (start_row + 1)] = colour
        pixels[(start_col) * stride + (start_row + 3)] = colour

def clock():
    while True:
        year, month, day, hour, minute, second, *_ = datetime.now().timetuple()

        print(hour)
        s_hour = str(hour)
        if len(s_hour) == 1:
            h0 = "0"
            h1 = s_hour[0]
        else:
            h0, h1 = s_hour

        print(minute)
        s_min = str(minute)
        if len(s_min) == 1:
            m0 = "0"
            m1 = s_min[0]
        else:
            m0, m1 = s_min

        print(second)
        s_sec = str(second)
        if len(s_sec) == 1:
            s0 = "0"
            s1 = s_sec[0]
        else:
            s0, s1 = s_sec

        delay = 0.1

        digit(pixels, h0, 2, 0)
        digit(pixels, h1, 2, 4)

        # digit(pixels, ":", 2, 7)

        digit(pixels, m0, 2, 8)
        digit(pixels, m1, 2, 12)

        digit(pixels, s0, 10, 6)
        digit(pixels, s1, 10, 10)

        pixels.show()
        time.sleep(delay)
        
        # reset()
        # pixels.show()
        # time.sleep(delay/2)


if __name__ == "__main__":
    # line(delay=0.1)
    # clock()

    read_from_std_in(delay = 2) 


