# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

# Simple test for NeoPixels on Raspberry Pi
import time
import board
import neopixel
import math
import sys

import fileinput
from datetime import datetime

# Choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D18
# NeoPixels must be connected to D10, D12, D18 or D21 to work.
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 256

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

# max_brightness = 0.005
# max_brightness = 0.02
# max_brightness = 0.1
max_brightness = 0.03
# max_brightness = 0.04

pixels = neopixel.NeoPixel(
        pixel_pin, num_pixels, brightness=max_brightness, auto_write=False, pixel_order=ORDER
        )
def reset():
    for i in range(num_pixels):
        pixels[i] = (0,0,0)

def line(delay):
    counter = 0
    while True:
        for i in range(num_pixels):
            pixels[i] = (0,0,0)
            if i == counter:
                pixels[i] = (255, 0,0)

        pixels.show()
        time.sleep(delay)
        counter += 1
        if counter == num_pixels:
            counter = 0


def read_from_std_in(delay):

    counter = 0
    for line in fileinput.input():
        # print(line)
        rgb = tuple(map(int, line.split()))
        # print(rgb)
        pixels[counter] = rgb
        counter += 1
        if counter == num_pixels:
            counter = 0
            pixels.show()
            time.sleep(delay)

    pixels.show()
    time.sleep(1)

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)


def makeColorGradient(idx, frequency1, frequency2, frequency3,
        phase1, phase2, phase3,
        center = 128, width = 127):
    red = math.sin(frequency1 * idx + phase1) * width + center;
    grn = math.sin(frequency2 * idx + phase2) * width + center;
    blu = math.sin(frequency3 * idx + phase3) * width + center;
    return (int(red), int(grn), int(0.9*blu))

def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.show()
        time.sleep(wait)


def digit(pixels, number, start_row, start_col, colour):
    stride = 16
    flip = stride - 1
    off = (0,0,0)
    do_flip = False
    if start_col % 2 == 1:
        do_flip = True

    def rev_idx(idx):
        if do_flip:
            return flip - idx
        else:
            return idx
    def inv_rev_idx(idx):
        if do_flip:
            return idx
        else:
            return flip - idx



    for j in range(5):
        pixels[(start_col + 0) * stride + rev_idx(start_row + j)] = off
        pixels[(start_col + 1) * stride + inv_rev_idx(start_row + j)] = off
        pixels[(start_col + 2) * stride + rev_idx(start_row + j)] = off
    # colour = (255,0,0)
    # colour = (0,220,0)

    
    if number == 0 or number == "0":
        """
        ### 
        # #
        # #
        # #
        ###
        """

        """
        for i in range(5):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 4):
            pixels[(start_col + 1) * stride +  inv_rev_idx(start_row + i)] = colour

        for i in range(5):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour
        """

        """
         ##
        # #
        # #
        # #
        ##
        """
        for i in range(1,5):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 4):
            pixels[(start_col + 1) * stride +  inv_rev_idx(start_row + i)] = colour

        for i in range(0,4):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour


    elif number == 1 or number == "1":
        """
        ##  
         #
         #
         #
        ###
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(5):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        pixels[(start_col + 2) * stride + rev_idx(start_row + 4)] = colour

        """
         #
        ##
         #
         #
        ###
        """
        """
        for i in range(1, 5, 3):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(5):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        pixels[(start_col + 2) * stride + rev_idx(start_row + 4)] = colour
        """


    elif number == 2 or number == "2":
        """
        ##
          #
         #
        # 
        ###
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour
        pixels[start_col * stride + rev_idx(start_row + 3)] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(1, 5, 3):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == 3 or number == "3":
        """
        ###
          #
         #
          #
        ###
        """
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour
        pixels[(start_col + 2) * stride + rev_idx(start_row + 2)] = off
        """

        """
        ##
          #
         #
          #
        ##
        """
        for i in range(0, 5, 4):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(1, 5, 2):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == 4 or number == "4":
        """
        # #
        # #
        ###
          #
          #
        """
        for i in range(0, 3, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(2, 3, 1):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour


    elif number == 5 or number == "5":
        """
        ###
        #
        ##
          #
        ##  
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour
        pixels[start_col * stride + rev_idx(start_row + 3)] = off

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 4, 3):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == 6 or number == "6":
        """
        ###
        #
        ###
        # #
        ###
        """

        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour
        pixels[(start_col + 2) * stride + rev_idx(start_row + 1)] = off
        """

        """
         ##
        #
        ###
        # #
        ###
        """

        for i in range(1, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour
        pixels[(start_col + 2) * stride + rev_idx(start_row + 1)] = off


    elif number == 7 or number == "7":
        """
        ###
          #
         #
        #
        #
        """
        for i in range(3, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour
        pixels[start_col * stride + rev_idx(start_row)] = colour

        for i in range(0, 3, 2):
            pixels[(start_col + 1) * stride +  inv_rev_idx(start_row + i)] = colour

        for i in range(0, 2, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == 8 or number == "8":
        """
        ###
        # #
        ###
        # #
        ###
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour
        
    elif number == 9 or number == "9":
        """
        ###
        # #
        ###
          #
          #
        """
        """
        for i in range(0, 3, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 3, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

        """
        """
        ###
        # #
        ###
          #
        ##
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour
        pixels[start_col * stride + rev_idx(start_row + 3)] = off

        for i in range(0, 5, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 4, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour


    elif number == ":":
        """
         #   

         #

        """
        pixels[(start_col) * stride + rev_idx(start_row + 1)] = colour
        pixels[(start_col) * stride + rev_idx(start_row + 3)] = colour
    
    elif number == "a":
        """
        
        ##
         ##
        # #
        ###
        """
        for i in range(1, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour
        pixels[start_col * stride + rev_idx(start_row + 2)] = off

        for i in range(1, 5, 1):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour
        pixels[(start_col + 1)* stride + inv_rev_idx(start_row + 3)] = off

        for i in range(2, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == "A":
        """
         #
        # #
        ###
        # #
        # #
        """
        for i in range(1, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 3, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(1, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour


    elif number == "p":
        """

        ##
        # #
        ##
        #
        """
        for i in range(1, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(1, 4, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(2, 3, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == "P":
        """
        ##
        # #
        ## 
        #
        #
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(0, 3, 2):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(1, 2, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == "m":
        """

        # #
        ###
        ###
        # #
        """
        for i in range(1, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(2, 4, 1):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(1, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    elif number == "M":
        """
        # #
        ###
        ###
        # #
        # #
        """
        for i in range(0, 5, 1):
            pixels[start_col * stride + rev_idx(start_row + i)] = colour

        for i in range(1, 3, 1):
            pixels[(start_col + 1) * stride + inv_rev_idx(start_row + i)] = colour

        for i in range(0, 5, 1):
            pixels[(start_col + 2) * stride + rev_idx(start_row + i)] = colour

    else:
        return

def clock():
    counter = 0
    last_sec = 0
    while True:
        year, month, day, hour, minute, second, *_ = datetime.now().timetuple()
        if last_sec != second:
            last_sec = second

            # if counter % 20 < 10:
            #     l0 = "a"
            #     l1 = "m"
            # else:
            #     l0 = "A"
            #     l1 = "M"

            # if hour > 12:
            #     hour = hour % 12

            #     if counter % 20 < 10:
            #         l0 = "p"
            #         l1 = "m"
            #     else:
            #         l0 = "P"
            #         l1 = "M"
            nighttime = False
            if(hour > 19 or hour < 7):
                nighttime = True

            l0 = "a"
            l1 = "m"
            if hour > 12:
                hour = hour % 12

                l0 = "p"
                l1 = "m"
            
            # print(hour)
            s_hour = str(hour)
            if len(s_hour) == 1:
                h0 = "0"
                h1 = s_hour[0]
            else:
                h0, h1 = s_hour

            # print(minute)
            s_min = str(minute)
            if len(s_min) == 1:
                m0 = "0"
                m1 = s_min[0]
            else:
                m0, m1 = s_min

            # print(second)
            s_sec = str(second)
            if len(s_sec) == 1:
                s0 = "0"
                s1 = s_sec[0]
            else:
                s0, s1 = s_sec

            #colour = wheel(counter)

            # # reg rainbow
            # center = 128
            # width = 127

            # # pastels 
            # center = 230
            # width = 25

            # # more saturated pastels 
            # center = 200
            # width = 55

            # idk
            center = 170
            width = 85

            # ccheck if night light mode, dim if 
            if(nighttime):
                center = 35
                width = 20

            freq = 0.3 # high freq
            freq = 0.1 # low freq 
            freq = 0.01 # lower freq 

            colour = makeColorGradient(counter, freq, freq, freq, 0, 2, 4, center, width)
            counter += 1
            if(counter >= sys.maxsize - 1):
                counter = 0

            print(counter, colour)

            digit(pixels, h0, 2, 0, colour)
            digit(pixels, h1, 2, 4, colour)

            """
            if counter % 2 == 0:
                digit(pixels, ":", 2, 7, colour)
                digit(pixels, ":", 2, 8, colour)
            else:
                digit(pixels, ":", 2, 7, (0,0,0))
                digit(pixels, ":", 2, 8, (0,0,0))
            """

            digit(pixels, m0, 2, 9, colour)
            digit(pixels, m1, 2, 13, colour)

            digit(pixels, s0, 9, 0, colour)
            digit(pixels, s1, 9, 4, colour)

            digit(pixels, l0, 9, 9, colour)
            digit(pixels, l1, 9, 13, colour)

            pixels.show()
            time.sleep(0.2)
            
            # reset()
            # pixels.show()
            # time.sleep(delay/2)


if __name__ == "__main__":
    # line(delay=0.1)

    # read_from_std_in(delay = 2) 

    clock()

