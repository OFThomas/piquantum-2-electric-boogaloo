/**
 * @author      : oli (oli@oli-HP)
 * @file        : maths
 * @created     : Saturday Jun 18, 2022 20:20:14 BST
 */

#ifndef PIQ_MATHS_HPP
#define PIQ_MATHS_HPP

#include <vector>
#include <iostream>

#include <cmath>
#include <complex>

namespace PiQ
{
    namespace maths
    {

        template <typename float_t>
            class Displacement_vector 
            {
                private:
                public:
                    std::vector<float_t> vector;

                    // Displacement_vector() = default;
                    Displacement_vector(size_t dim) : vector(std::vector<float_t>(dim, 0)) {}

                    size_t size() const
                    {
                        return vector.size();
                    }
            };

        template <typename float_t>
            class Heap_matrix 
            {
                public:
                    std::vector<float_t> values = std::vector<float_t>(4,0);
            };

        template <typename float_t>
            class Stack_matrix 
            {
                public:
                    float_t values[4];
            };

        template <typename float_t, template<typename> typename matrix_t>
            class Matrix 
            {
                public:
                    const char stride = 2;
                    matrix_t<float_t> matrix;

                    /*
                       float_t operator()(size_t row, size_t col) const
                       {
                       return matrix.values[row + col*stride];
                       }
                       */

                    float_t & operator()(size_t row, size_t col) 
                    {
                        return matrix.values[row + col*stride];
                    }

                    const float_t & operator()(size_t row, size_t col) const
                    {
                        return matrix.values[row + col*stride];
                    }


                    void print() const 
                    {
                        for(size_t row = 0; row < stride; row++)
                        {
                            std::cout << "( ";
                            for(size_t col = 0; col < stride; col++)
                            {
                                std::cout << this->operator()(row, col) << " ";
                            }
                            std::cout << ")" << std::endl;
                        }
                    }
            };


        template <typename float_t = float>
            class Angle
            {
                public:
                    float_t value;
            };

        template <typename float_t = float>
            class Reflectivity
            {
                public:
                    float_t value;
            };

        template <typename float_t = float>
            class Transmission
            {
                public:
                    float_t value;
            };

        template <typename float_t = float, template<typename> typename matrix_t = Stack_matrix>
            class Beam_splitter 
            {
                private:
                    Beam_splitter(float_t val) 
                    {
                        // matrix(0,0) = 1.0 / std::sqrt(2.0);
                        // matrix(1,0) = -1.0 / std::sqrt(2.0);
                        // matrix(0,1) = -1.0 / std::sqrt(2.0);
                        // matrix(1,1) = 1.0 / std::sqrt(2.0);
                        matrix(0,0) = cos(val);
                        matrix(1,0) = -sin(val);
                        matrix(0,1) = sin(val);
                        matrix(1,1) = cos(val);
                    }
                public:
                    Matrix<float_t, matrix_t> matrix;

                    Beam_splitter(Angle<float_t> angle) : Beam_splitter(angle.value) {}

                    Beam_splitter(Reflectivity<float_t> R = Reflectivity<float_t>{0.5}) 
                        : Beam_splitter(1.0 * std::asin(std::sqrt(R.value))) 
                        {}
                    Beam_splitter(Transmission<float_t> T) 
                        : Beam_splitter(1.0 * std::acos(std::sqrt(T.value))) 
                    {}
            };

        template <typename float_t = float> 
            class Phase
            {
                public:
                    float_t value;

                    Phase(float_t in) : value(std::exp(in)) {}
            };

        // template<template<typename> typename matrix_t>
        // const float Beam_splitter<float, matrix_t>::make_matrix()
        // {
        //     Matrix<float, matrix_t> matrix;
        //             matrix(0,0) = 1.0f / std::sqrt(2.0f);
        //             matrix(1,0) = -1.0f / std::sqrt(2.0f);
        //             matrix(0,1) = -1.0f / std::sqrt(2.0f);
        //             matrix(1,1) = 1.0f / std::sqrt(2.0f);
        //             return matrix;
        //         }


        template <typename vector_t, typename op_t>
            void apply(vector_t & disp, const op_t & bs, size_t m0, size_t m1)
            {
                // take elements m0 and m1 from disp vector, multiply and write back 
                auto t0 = disp.vector[m0];
                auto t1 = disp.vector[m1];
                // std::cout << "SIZE OF t0 " << sizeof(t0) << std::endl;;

                // m * d = [a b] [m0] = [a*m0 + b*m1]
                //         [c d] [m1]   [c*m0 + d*m1]
                disp.vector[m0] = bs.matrix(0,0) * t0 + bs.matrix(0,1) * t1;
                disp.vector[m1] = bs.matrix(1,0) * t0 + bs.matrix(1,1) * t1;
                // std::cout << "SIZE OF BS.MATRIX(0,0) " << sizeof(bs.matrix(0,0)) << std::endl;
            }

        int benchmarks();


    } // end of math namespace 
} // end of PiQ namespace 

#endif // end of include guard MATHS_HPP 

