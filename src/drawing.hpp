/**
 * @author      : oli (oli@oli-HP)
 * @file        : drawing
 * @created     : Saturday Jun 18, 2022 20:24:32 BST
 */

#ifndef DRAWING_HPP
#define DRAWING_HPP

#include <iostream>
#include <olistd/maths>

namespace PiQ
{
    namespace Draw
    {

        class Point 
        {
            private:
            public:
                float x = 0.0;
                float y = 0.0;

                Point() = default;
                Point(float x, float y) : x(x), y(y) {}

                friend bool operator< (const Point & lhs, const Point & rhs) 
                {
                    float lhs_dist = olistd::sq(lhs.x) + olistd::sq(lhs.y);
                    float rhs_dist = olistd::sq(rhs.x) + olistd::sq(rhs.y);
                    return lhs_dist < rhs_dist;
                }

                float distance(const Point & rhs) const
                {
                    float lhs_dist = olistd::sq(x) + olistd::sq(y);
                    float rhs_dist = olistd::sq(rhs.x) + olistd::sq(rhs.y);
                    // return lhs_dist - rhs_dist;
                    // return std::sqrt(lhs_dist) - std::sqrt(rhs_dist);
                    float dx = x - rhs.x; 
                    float dy = y - rhs.y;
                    return std::sqrt(dx * dx + dy * dy);
                }

                friend std::ostream & operator<<(std::ostream & os, const Point & point)
                {
                    os << "(" << point.x << "," << point.y << ")";
                    return os;
                }
        };

        class Pixel
        {
            private:
            public:
                Point point;
                float value = 0.0;

                Pixel(const Point & point, bool value = 0.0) : point(point), value(value) {}

                std::string str() const
                {
                    return std::to_string(point.x) + " " + std::to_string(point.y) + " " + std::to_string(value);
                }
        };

        inline float gaussian(float x, float y, float x_0, float y_0, float sigma_x = 1.0, float sigma_y = 1.0)
        {
            // I don't know if M_PI is defined on the microcontroller.
            float tau = (2.0 * 3.14159265359);

            float A = 1.0 / (tau * sigma_x * sigma_y);
            float x_arg = 0.5 * olistd::sq(x - x_0) / olistd::sq(sigma_x);
            float y_arg = 0.5 * olistd::sq(y - y_0) / olistd::sq(sigma_y);
            float arg = - (x_arg + y_arg);
            float result = A * std::exp(arg);
            return result;
        }

        class Bounding_box 
        {
            private:
            public:
                std::vector<Point> points;
                Point bot_left;
                Point top_right;

                // calculating the min and max (boundary) of the shape.
                Bounding_box(const std::vector<Point> & points) : points(points)
            {
                // iterate through the points to find the min and max x 
                // and min andmax y values (4 in total) this defines 
                // the bounding box  
                float x_min = points.at(0).x;
                float x_max = x_min;
                float y_min = points.at(0).y;
                float y_max = y_min;
                for(const Point & point : points)
                {
                    x_min = point.x < x_min ? point.x : x_min;
                    x_max = x_max < point.x ? point.x : x_max;

                    y_min = point.y < y_min ? point.y : y_min;
                    y_max = y_max < point.y ? point.y : y_max;
                }

                // set the members of the rectangle base 
                bot_left = Point{x_min, y_min};
                top_right = Point{x_max, y_max};

                std::cout << "bot_left " << bot_left << ", top_right " << top_right << std::endl;
            }
                bool inside(const Point & point) const
                {
                    bool b_x = bot_left.x <= point.x && point.x <= top_right.x;
                    bool b_y = bot_left.y <= point.y && point.y <= top_right.y; 
                    return b_x && b_y; 
                } 

                bool operator()(const Point & point) const 
                {
                    std::cout << point;
                    if(inside(point))
                    {
                        std::cout << " inside" << std::endl;
                        return true;
                    }
                    else 
                    {
                        std::cout << " outside" << std::endl;
                        return false;
                    }
                }

                // return inside(point);
                // use the winding algorithm for convex shapes 
                bool convex_inside(const Point & point) const 
                {
                    return inside(point);
                    // check bounding box first 
                    if(not inside(point))
                    {
                        return false;
                    }
                    else
                    {
                        // go though the points in the shape to calc the edges 
                        // https://stackoverflow.com/a/2752753   //
                        for(size_t i = 0; i < points.size() - 1; i++)
                        {
                            float d0 = (points[i+1].x - points[i].x ) * (point.y - points[i].y);
                            float d1 = (point.x - points[i].x) * (points[i+1].y - points[i].y);
                            if(d0 - d1 < 0)
                                return false;
                        }
                        return true;
                    }
                }
        };

        // https://stackoverflow.com/a/218081
        class Ray_casting
        {
            private:
            public:
        };

        class Shape 
        {
            private:
            public:
                Bounding_box bbox;
                Shape(const std::vector<Point> & points) : bbox(points) {}
                float operator()(float x, float y);
        };

        class Rectangle : public Shape
        {
            private:
            public:
                Rectangle(const Point & p1, float width, float height) : Rectangle(p1, {p1.x, p1.y + height}, {p1.x + width, p1.y + height}, {p1.x + width, p1.y}) {}
                Rectangle(const Point & p1, const Point & p2, const Point & p3, const Point & p4) : Shape({p1,p2,p3,p4}) {}

                bool operator()(const Point & point) const 
                {
                    // convex so can use winding algo
                    return bbox.convex_inside(point);
                }
        };

        // A line
        //  #######
        //  or
        //  #
        //  #
        //  #
        //  or
        //  #
        //   #
        //    # 
        // or
        //      #
        //    #
        //  #
        class Line : public Shape
        {
            private:
            public:
                Point start;
                Point end;

                Line(const Point & start, const Point & end) : Shape({start, end}) {}
                float operator()(const Point & point) const
                {
                    return bbox.convex_inside(point);
                }

        };

        class Waveguide : public Rectangle
        {
            private:
            public:

                Waveguide(const Point & start, float width, float height) : Rectangle(start, width, height) {}


        };

        //     A circle if you will.
        //      # 
        //    #   #
        //      #     
        class Circle 
        {
            private:
            public:

                Point centre;
                float inner_radius;
                float thickness = 0.1;

                Circle(const Point & centre, float inner_radius, float thickness = 0.1) : centre(centre), inner_radius(inner_radius), thickness(thickness) {}

                float operator()(const Point & point) const
                {
                    float dist = centre.distance(point); 
                    std::cout << point;
              bool inside = inner_radius <= dist && dist <= inner_radius + thickness;
                    // std::cout << " dist " << dist << " inner " << inner_radius << " outer_rad " << thickness+inner_radius << " inside " << inside << std::endl;
                    return inside;
                }
        };

        // directional coupler (beamsplitter)
        // ##    ##       
        //    ##        
        //    ##        
        // ##    ##     
        // simply beautiful.
        class Directional_coupler
        {
            private:
            public:
                size_t width;
                size_t height;
                float thickness;

                Directional_coupler(size_t width, size_t height, float thickness = 0.1) : width(width), height(height), thickness(thickness) {}
                float operator()(const Point & point) const
                {


                    return false;
                }
        };

        // 
        using shape_t = std::variant<Line, Circle, Rectangle>;

        class LED_array
        {
            private:
            public:
                std::vector<Pixel> pixels;
                std::vector<shape_t> shapes;
                std::string path;

                LED_array(const std::vector<Pixel> & pixels) : pixels(pixels) {}
                LED_array(const std::vector<Pixel> & pixels, const std::string & path) : pixels(pixels), path(path) {}

                void write(const std::string & filename, bool norm = false)
                {
                    path = filename;
                    write(norm);
                }

                void write(bool norm = false)
                {
                    calc_pixel_values();
                    if(path == "")
                    {
                        for(const Pixel & pixel : pixels)
                            std::cout << pixel.str() << std::endl; 
                    }
                    else
                    {
                        std::ofstream file;
                        file.open(path);
                        for(const Pixel & pixel : pixels)
                            file << pixel.str() << std::endl; 

                        file.close();
                        std::string last_char = std::string{path[path.size() - 1]};
                        if(last_char != "t")
                        {
                            int next_idx = std::stoi(last_char) + 1;
                            path.replace(path.size() - 1, 1, std::to_string(next_idx));
                        }
                    }
                }

                void add(const shape_t & shape)
                {
                    shapes.push_back(shape);
                }

                void clear()
                {
                    shapes.clear();
                }

                void set(const shape_t & shape)
                {
                    clear();
                    add(shape);
                }

                void calc_pixel_values()
                {
                    for(Pixel & pixel : pixels)
                    {
                        // sum shape contributions
                        bool val = false;
                        for(const auto & shape : shapes)
                        {
                            std::visit([&](auto && l_shape) 
                                    {
                                    val |= l_shape(pixel.point) != 0 ? 1 : 0;
                                    }, shape);
                        }
                        pixel.value = val;
                    }
                }

                void draw()
                {
                    calc_pixel_values();
                    for(Pixel & pixel : pixels)
                        std::cout << pixel.str() << std::endl;
                }
                void write_rgb()
                {
                    calc_pixel_values();
                    for(const Pixel & pixel : pixels)
                        std::cout << 255 * pixel.value << " " << 200 * pixel.value << " " << "0" << std::endl;
                }

        };

        // layout 
        class LED_grid : public LED_array
        {
            private:
                std::vector<Pixel> set_pixels(size_t width, size_t height, size_t xoff, size_t yoff)
                {
                    std::vector<Pixel> pixels;
                    for(float h = 0; h < height; h++)
                        for(float w = 0; w < width; w++)
                            pixels.push_back(Point{w + xoff, h + yoff});
                    return pixels;
                }

            public:

                LED_grid(size_t width, size_t height, const std::string & path = "") : LED_array(set_pixels(width, height, 0, 0), path) {}
                LED_grid(size_t width, size_t height, size_t xoff, size_t yoff, const std::string & path = "") : LED_array(set_pixels(width, height, xoff, yoff), path) {}

        };

        class LED_hex : public LED_array
        {
            private:
                std::vector<Pixel> set_pixels(size_t width, size_t height, float padding)
                {
                    // hex algo from
                    // https://stackoverflow.com/questions/20734438/algorithm-to-generate-a-hexagonal-grid-with-coordinate-system
                    float ang30 = 30.0 * M_PI / 180.0;
                    float xOff = std::cos(ang30) * (width / 2 + padding);
                    float yOff = std::sin(ang30) * (height / 2 + padding);
                    int half = height / 2;

                    float norm = (width * height);
                    std::vector<Pixel> pixels;
                    for (int row = 0; row < height; row++)
                    {
                        int cols = height - std::abs(row - half);
                        for (int col = 0; col < cols; col++)
                        {
                            // int xLbl = row < half ? col - row : col - half;
                            // int yLbl = row - half;
                            float x_cen = 0;
                            float y_cen = 0;
                            // float x_cen = (float)width / 2.0;
                            // float y_cen = (float)height / 2.0;
                            float x = (x_cen + xOff * (col * 2 + 1 - cols));
                            float y = (y_cen + yOff * (row - half) * 3);

                            // drawHex(g, xLbl, yLbl, x, y, radius);
                            Point point{x / norm + 0.5f, y / norm + 0.5f};
                            pixels.push_back(point);
                        }
                    }
                    return pixels;
                }

                // float a = spacing;
                // std::vector<Pixel> pixels;
                // for(int h = 0; h < height; h++)
                // {
                //     for(int w = 0; w < width; w++)
                //     {
                //         float x = (a / 2.0) * w;
                //         float y = (std::sqrt(3.0) / 2.0) * a * w + std::sqrt(3.0) * a* h;
                //         pixels.push_back(Point{x,y});
                //     }
                // }
                // return pixels;
                // }

        public:
                LED_hex(size_t width, size_t height, const std::string & path = "", float spacing = 1.0) : LED_array(set_pixels(width, height, spacing), path){}
    };


} // end of Draw namespace 
} // end of PiQ namespace 

#endif // end of include guard DRAWING_HPP 

