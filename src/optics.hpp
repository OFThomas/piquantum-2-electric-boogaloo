/**
 * @author      : oli (oli@oli-HP)
 * @file        : optics
 * @created     : Saturday Jun 18, 2022 20:23:22 BST
 */

#ifndef OPTICS_HPP
#define OPTICS_HPP

#include <vector>

namespace PiQ
{
    namespace optics
    {
        // A waveguide has a length and vector of values, i.e. what to display
        // at each interval 
        // there are two approaches that come to mind initially
        // consider the sequence (R = Red, G = Green, B = Blue, W = White):
        // R R W G B W W G
        // 1). The vector is of fixed length and each tile increments a counter
        // to read the next value 
        // 2). we compress the data and each element has a start and stop counter 
        // Let's just use 1). for now.
        // this class pads 

        namespace BASE
        {
            template <typename data_t>
                class Waveguide
                {
                    private:
                    public:
                        size_t full_size;
                        std::vector<data_t> values;

                        // zero init full vector 
                        Waveguide(size_t size) : full_size(size), values(std::vector<data_t>(full_size, 0)) {}

                        // write full vector 
                        Waveguide(const std::vector<data_t> & vals) : full_size(vals.size()), values(vals) {}


                        size_t size() const
                        {
                            return full_size;
                        }

                        bool set(size_t idx, char value);
                        // i.e. for sending to display etc
                        std::vector<data_t> write();
                };
        } // end of BASE namespace

        // the full waveguide (plain vector of all values)
        // oh See this for why we need this-> everywhere, 
        // https://stackoverflow.com/questions/12032345/protected-member-is-not-declared-in-this-scope-in-derived-class
        //
        // and this
        // https://isocpp.org/wiki/faq/templates#nondependent-name-lookup-members
        //
        // The waveguide class interpolates values between known, set ones 
        template <typename data_t>
            class Waveguide : public BASE::Waveguide<data_t>
        {
            private:
                size_t prev_idx = 0;
                void propogate_to(size_t idx)
                {
                    // for points between prev_idx and here propogate the values forward
                    // then need to update all from prev_idx to idx
                    for(size_t i = prev_idx + 1; i < idx; i++)
                        this->values[i] = this->values[prev_idx];

                    prev_idx = idx;
                }
            public:
                // zero init full vector 
                Waveguide(size_t size) : BASE::Waveguide<data_t>(size) {} 
                // write full vector 
                Waveguide(const std::vector<data_t> & vals) : BASE::Waveguide<data_t>(vals) {}

                bool set(size_t idx, data_t value)
                {
                    // probs need to bounds checking 
                    if(idx > this->full_size - 1 or idx >= this->values.size())
                        std::cerr << "Err Waveguide set out of bounds " << std::endl;

                    // for points between prev_idx and here propogate the values forward
                    // then need to update all from prev_idx to idx
                    propogate_to(idx);
                    this->values[idx] = value;

                    return true;
                }

                bool set(const std::vector<size_t> & idxs, std::vector<data_t> & data)
                {
                    if(idxs.size() != data.size())
                        std::cerr << "ERR idxs and data size not equal in waveguide.set(...)" << std::endl;
                    for(size_t i = 0; i < idxs.size(); i++)
                        set(idxs[i], data[i]);

                    return true;
                }

                std::vector<data_t> write() 
                {
                    if(prev_idx != this->full_size - 1)
                        propogate_to(this->full_size);

                    return this->values;
                }
        };


    }
}


#endif // end of include guard OPTICS_HPP 

