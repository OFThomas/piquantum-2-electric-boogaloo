# to plot with gnuplot the led grids, 
 plot "test.dat" w p pt 6 palette, "hex.dat" w p pt 5 palette

# To plot gifs
plot "grid.dat0" w p pt 2 palette, "grid.dat1" w p pt 6 palette

sudo python3 neopixels.py < data.txt
