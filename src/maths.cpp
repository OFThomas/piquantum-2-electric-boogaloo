/**
 * @author      : oli (oli@oli-HP)
 * @file        : maths
 * @created     : Saturday Jun 18, 2022 20:21:17 BST
 */

#include "maths.hpp"
#include "optics.hpp"

#include <iostream>

#include <olistd/io>
#include <olistd/vector>
#include <olistd/timer>
#include <olistd/maths>

namespace PiQ
{
    namespace maths
    {
        int benchmarks()
        {
            std::cout << "hi" << std::endl;

            std::vector<int> rand = olistd::random_vector<int>(10);
            olistd::print(rand);
            {
                Beam_splitter<float> bs;
                bs.matrix.print();
                std::cout << "size of " << sizeof(bs) << std::endl;
            }
            {
                Beam_splitter<double> bs;
                bs.matrix.print();
                std::cout << "size of " << sizeof(bs) << std::endl;
            }

            {
                Beam_splitter<float, Stack_matrix> bs;
                bs.matrix.print();
                std::cout << "size of " << sizeof(bs) << std::endl;
            }

            {
                Beam_splitter<double, Stack_matrix> bs;
                bs.matrix.print();
                std::cout << "size of " << sizeof(bs) << std::endl;
            }

            {
                Beam_splitter<float, Heap_matrix> bs;
                bs.matrix.print();
                std::cout << "size of " << sizeof(bs) << std::endl;
            }

            {
                Beam_splitter<double, Heap_matrix> bs;
                bs.matrix.print();
                std::cout << "size of " << sizeof(bs) << std::endl;
            }

            const size_t dim = 100;
            const size_t num_layers = 1e5;
            {
                olistd::Timer time;
                Displacement_vector<float> state(dim);
                state.vector[0] = 1.0;
                olistd::print(state.vector);
                std::cout << "size of state" << sizeof(state) << std::endl;

                for(size_t op = 0; op < num_layers; op++)
                {
                    for(size_t modes = 0; modes < state.size()- 1; modes++)
                    {
                        Beam_splitter<float> bs;
                        apply(state, bs, modes, modes+1);
                    }
                    Beam_splitter<float> bs;
                    apply(state, bs, state.size()-1, 0);
                    // olistd::print(state.vector);
                }

                std::cout << "after ops " << std::endl;
                olistd::print(state.vector);
                std::cout << "-------- float stack -------\n";
                time.stop();
                std::cout << "-----------------------------" << std::endl;
            }
            {
                olistd::Timer time;
                Displacement_vector<float> state(dim);
                state.vector[0] = 1.0;
                olistd::print(state.vector);
                std::cout << "size of state" << sizeof(state) << std::endl;

                for(size_t op = 0; op < num_layers; op++)
                {
                    for(size_t modes = 0; modes < state.size()- 1; modes++)
                    {
                        Beam_splitter<float, Heap_matrix> bs;
                        apply(state, bs, modes, modes+1);
                    }
                    Beam_splitter<float, Heap_matrix> bs;
                    apply(state, bs, state.size()-1, 0);
                    // olistd::print(state.vector);
                }

                std::cout << "after ops " << std::endl;
                olistd::print(state.vector);
                std::cout << "-------- float heap allocated -------\n";
                time.stop();
                std::cout << "-------------------------------------" << std::endl;
            }

            {
                olistd::Timer time;
                Displacement_vector<double> state(dim);
                state.vector[0] = 1.0;
                olistd::print(state.vector);
                std::cout << "size of state" << sizeof(state) << std::endl;

                for(size_t op = 0; op < num_layers; op++)
                {
                    for(size_t modes = 0; modes < state.size()-1; modes++)
                    {
                        Beam_splitter<double> bs;
                        apply(state, bs, modes, modes+1);
                    }
                    Beam_splitter<double> bs;
                    apply(state, bs, state.size()-1, 0);
                    // olistd::print(state.vector);
                }

                std::cout << "after ops " << std::endl;
                olistd::print(state.vector);
                std::cout << "-------- double stack -------\n";
                time.stop();
                std::cout << "-----------------------------" << std::endl;
            }
            {
                olistd::Timer time;
                Displacement_vector<double> state(dim);
                state.vector[0] = 1.0;
                olistd::print(state.vector);
                std::cout << "size of state" << sizeof(state) << std::endl;

                for(size_t op = 0; op < num_layers; op++)
                {
                    for(size_t modes = 0; modes < state.size()-1; modes++)
                    {
                        Beam_splitter<double, Heap_matrix> bs;
                        apply(state, bs, modes, modes+1);
                    }
                    Beam_splitter<double, Heap_matrix> bs;
                    apply(state, bs, state.size()-1, 0);
                    // olistd::print(state.vector);
                }

                std::cout << "after ops " << std::endl;
                olistd::print(state.vector);
                std::cout << "-------- double heap allocated -------\n";
                time.stop();
                std::cout << "--------------------------------------" << std::endl;


            }

            return 0;
        }


    } // end of math namespace
} // end of  PiQ namespace 


